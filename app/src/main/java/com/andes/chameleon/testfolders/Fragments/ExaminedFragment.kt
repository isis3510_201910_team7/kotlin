package com.andes.chameleon.testfolders.Fragments

import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import com.andes.chameleon.testfolders.ChooseProtocolActivity
import com.andes.chameleon.testfolders.CreatePatientActivity
import com.andes.chameleon.testfolders.EditExaminedActivity
import com.andes.chameleon.testfolders.DataModel.PatientPull
import com.andes.chameleon.testfolders.R
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.database.*

class ExaminedFragment : Fragment() {

    private lateinit var patientsRef : DatabaseReference
    private lateinit var examinedPopup : Dialog

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view : View = inflater.inflate(R.layout.fragment_examined, container, false)
        examinedPopup = Dialog(activity)
        val btnCreatePatient = view.findViewById<FloatingActionButton>(R.id.fabAddExamined)
        val dataList: RecyclerView = view.findViewById(R.id.patients_list)
        dataList.layoutManager = LinearLayoutManager(context)

        patientsRef = FirebaseDatabase.getInstance().reference.child("pacientes")

        val option = FirebaseRecyclerOptions.Builder<PatientPull>()
            .setQuery(patientsRef, PatientPull::class.java)
            .build()

        val nAdapter = object : FirebaseRecyclerAdapter<PatientPull, PatientsViewHolder>(option){
            override fun onCreateViewHolder(p0: ViewGroup, p1: Int): PatientsViewHolder {
                val view1 : View = LayoutInflater.from(p0.context).inflate(R.layout.patient_card_item, p0, false)

                return PatientsViewHolder(view1)
            }

            override fun onBindViewHolder(holder: PatientsViewHolder, position: Int, model: PatientPull) {

                val pacienteIds : String = getRef(position).key!!
                lateinit var pIdCedula : String
                lateinit var pName: String
                lateinit var pBirthDate: String
                lateinit var pEducation: String
                lateinit var pGender: String

                val listener = object : ValueEventListener{
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        pIdCedula = dataSnapshot.child("id").value.toString()
                        pName = dataSnapshot.child("name").value.toString()
                        pBirthDate = dataSnapshot.child("birthdate").value.toString()
                        pEducation = dataSnapshot.child("education").value.toString()
                        pGender = dataSnapshot.child("gender").value.toString()

                        holder.patientName.text = pName
                        holder.patientId.text = pIdCedula

                        if(pGender == "Masculino" || pGender == "Hombre"){
                            holder.patientPic.setImageResource(R.drawable.sample_user)
                        } else {
                            holder.patientPic.setImageResource(R.drawable.girl)
                        }
                    }

                    override fun onCancelled(databaseError: DatabaseError) {
                        println("loadPost:onCancelled ${databaseError.toException()}")
                    }
                }

                holder.itemView.setOnClickListener{
                    showPopup(pName, pIdCedula, pBirthDate, pEducation, pacienteIds, pGender)
                }

                patientsRef.child(pacienteIds).addValueEventListener(listener)
            }

        }
        dataList.adapter = nAdapter
        nAdapter.startListening()

        btnCreatePatient.setOnClickListener{
            val intent = Intent (context, CreatePatientActivity::class.java)
            startActivity(intent)
        }
        // Inflate the layout for this fragment
        return view
    }

    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(uri: Uri)
    }

    private fun showPopup(name: String, id: String, birthdate: String, education: String, pacienteID: String, gender:String){
        examinedPopup.setCancelable(false)
        examinedPopup.setContentView(R.layout.examined_popup)
        val pName = examinedPopup.findViewById(R.id.popupName) as TextView
        val pID = examinedPopup.findViewById(R.id.popupID) as TextView
        val pBirthdate = examinedPopup.findViewById(R.id.popupBirthdate) as TextView
        val pEducation = examinedPopup.findViewById(R.id.popupEducation) as TextView
        val pGender = examinedPopup.findViewById(R.id.popupGender) as TextView
        val pPic = examinedPopup.findViewById<ImageView>(R.id.imageView3)

        pName.text = name
        pID.text = id
        pBirthdate.text = birthdate
        pEducation.text = education
        pGender.text = gender

        if(gender == "Masculino" || gender == "Hombre"){
            pPic.setImageResource(R.drawable.sample_user)
        } else {
            pPic.setImageResource(R.drawable.girl)
        }

        val closeBtn = examinedPopup.findViewById(R.id.btnCerrarPopup) as TextView
        val applyBtn = examinedPopup.findViewById(R.id.btnAplicarPopup) as Button
        val editBtn = examinedPopup.findViewById(R.id.btnEditarPopup) as Button
        val deleteBtn = examinedPopup.findViewById(R.id.btnEliminarPopup) as ImageButton

        applyBtn.setOnClickListener {
            val chooseProtocolIntent = Intent (context, ChooseProtocolActivity::class.java)
            chooseProtocolIntent.putExtra(EXAMINED_ID, pacienteID)
            chooseProtocolIntent.putExtra(EXAMINED_NAME, name)
            examinedPopup.dismiss()
            startActivity(chooseProtocolIntent)
        }
        editBtn.setOnClickListener{
            val intent = Intent (context, EditExaminedActivity::class.java)
            intent.putExtra(EXAMINED_ID, pacienteID)
            intent.putExtra(EXAMINED_ID_CEDULA, id)
            intent.putExtra(EXAMINED_NAME, name)
            intent.putExtra(EXAMINED_BIRTHDATE, birthdate)
            intent.putExtra(EXAMINED_EDUCATION, education)
            intent.putExtra(EXAMINED_GENDER, gender)
            examinedPopup.dismiss()
            startActivity(intent)
        }
        deleteBtn.setOnClickListener{
            val builder = AlertDialog.Builder(activity)

            // Set the alert dialog title
            builder.setTitle("Eliminar examinado")

            // Display a message on alert dialog
            builder.setMessage("¿Está seguro de eliminar el examinado?")

            // Set a positive button and its click listener on alert dialog
            builder.setPositiveButton("Si"){dialog, which ->
                // Do something when user press the positive button
                patientsRef.child(pacienteID).removeValue()
                examinedPopup.dismiss()
            }

            // Display a negative button on alert dialog
            builder.setNegativeButton("No"){dialog,which ->
            }

            // Finally, make the alert dialog using builder
            val dialog: AlertDialog = builder.create()

            // Display the alert dialog on app interface
            dialog.show()
        }

        closeBtn.setOnClickListener { examinedPopup.dismiss() }
        examinedPopup.window.setBackgroundDrawableResource(R.color.colorInvisible)
        examinedPopup.show()
    }

    companion object {
        const val EXAMINED_ID = "com.andes.chameleon.testfolders.ID_EXAMINED"
        const val EXAMINED_ID_CEDULA = "com.andes.chameleon.testfolders.ID_CEDULA_EXAMINED"
        const val EXAMINED_NAME = "com.andes.chameleon.testfolders.NAME_EXAMINED"
        const val EXAMINED_BIRTHDATE = "com.andes.chameleon.testfolders.BIRTHDATE_EXAMINED"
        const val EXAMINED_EDUCATION = "com.andes.chameleon.testfolders.EDUCATION_EXAMINED"
        const val EXAMINED_GENDER = "com.andes.chameleon.testfolders.EDUCATION_GENDER"
    }
    class PatientsViewHolder(itemView:View): RecyclerView.ViewHolder(itemView) {
        var patientName : TextView = itemView.findViewById(R.id.patient_card_name)
        var patientId : TextView = itemView.findViewById(R.id.patient_card_id)
        var patientPic: ImageView = itemView.findViewById(R.id.patient_card_img)
    }
}
