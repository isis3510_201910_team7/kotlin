package com.andes.chameleon.testfolders

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.widget.*
import com.andes.chameleon.testfolders.DataModel.QualificationTool
import com.google.firebase.database.FirebaseDatabase

class CreateQualificationToolActivity : AppCompatActivity() {

    private lateinit var rutaBD: String
    private lateinit var broadRec: BroadcastReceiver

    private lateinit var tipoTool: RadioGroup
    private lateinit var visibilidad: Switch
    private lateinit var btnCrear: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_qualification_tool)

        rutaBD = intent.getStringExtra(CreateItemActivity.RUTA_BD)

        tipoTool = findViewById(R.id.radioGroup2)
        visibilidad = findViewById(R.id.swVisibilidadQualifTool)
        btnCrear = findViewById(R.id.btnCrearQualificationTool)

        btnCrear.setOnClickListener{
            saveQualificationTool()
        }

    }

    private fun saveQualificationTool(){
        val tipo = findViewById<RadioButton>(tipoTool.checkedRadioButtonId).text.toString().trim()

        val visibility = visibilidad.isChecked

        val ref = FirebaseDatabase.getInstance().getReference("tests/$rutaBD")
        val toolId = ref.push().key

        val nuevoTool = QualificationTool(toolId!!, tipo, visibility, TestsActivity.QUALIFICATION_TOOL)

        ref.child(toolId).setValue(nuevoTool).addOnCompleteListener{
            //Toast.makeText(applicationContext, "¡Tool de calificación guardado!", Toast.LENGTH_LONG).show()
        }

        finish()
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE)
        return if (connectivityManager is ConnectivityManager) {
            val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
            networkInfo?.isConnected ?: false
        } else false
    }

    override fun onStart() {
        super.onStart()

        broadRec = object : BroadcastReceiver() {

            override fun onReceive(context: Context, intent: Intent) {
                if(!isNetworkAvailable())
                {
                    val snackbar = Snackbar.make(findViewById(R.id.qualificationActivity),"Sin conexión, los datos se actualizarán una vez vuelva la conexión", Snackbar.LENGTH_INDEFINITE)
                    snackbar.setAction("Aceptar") { snackbar.dismiss() }

                    snackbar.show()
                }
            }
        }

        registerReceiver(broadRec, IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"))
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(broadRec)
    }
}
