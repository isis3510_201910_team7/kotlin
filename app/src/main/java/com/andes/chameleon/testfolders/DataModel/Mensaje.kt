package com.andes.chameleon.testfolders.DataModel

class Mensaje (val id: String, val nombre: String, val mensaje: String, val visibleExaminado: Boolean, val tipo: String)