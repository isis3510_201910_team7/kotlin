package com.andes.chameleon.testfolders

import android.app.Activity
import android.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.os.Bundle

class ApplyMessageActivity : AppCompatActivity() {

    private lateinit var mensaje: String
    private lateinit var nombreMensaje: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_apply_message)

        mensaje = intent.getStringExtra(TestsActivity.MENSAJE)
        nombreMensaje = intent.getStringExtra(TestsActivity.NOMBRE_MENSAJE)

        showPopup()
    }

    private fun siguiente()
    {
        setResult(Activity.RESULT_OK)
        finish()
    }

    private fun showPopup(){

        val builder = AlertDialog.Builder(this)

        builder.setTitle(nombreMensaje)
        builder.setMessage(mensaje)

        builder.setPositiveButton("Aceptar"){ _, _ ->
            siguiente()
        }

        val dialog: AlertDialog = builder.create()

        dialog.show()
    }
}
