package com.andes.chameleon.testfolders.DataModel

//Estructura para crear items.

class Item(val id: String, val nombre: String, val simpleComplejo: String, val tipo: String)