package com.andes.chameleon.testfolders

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.widget.Button
import android.widget.EditText
import android.widget.Switch
import android.widget.Toast
import com.andes.chameleon.testfolders.DataModel.Item
import com.google.firebase.database.FirebaseDatabase

class CreateItemActivity : AppCompatActivity() {

    private lateinit var rutaBD: String
    private lateinit var broadRec: BroadcastReceiver
    private lateinit var btnCrear: Button
    private lateinit var tipoItem: Switch
    private lateinit var nombreItem: EditText
    private lateinit var idTest: String
    private lateinit var idSubTest: String
    private lateinit var nameTest: String
    private lateinit var nameSubTest: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_item)

        btnCrear = findViewById(R.id.btnCrearItem)
        tipoItem = findViewById(R.id.swTipoItem)
        nombreItem = findViewById(R.id.txtNombreItem)

        idTest = intent.getStringExtra(EditTestActivity.TEST_ID)
        idSubTest = intent.getStringExtra(EditSubTestActivity.SUBTEST_ID)

        nameTest = intent.getStringExtra(EditTestActivity.TEST_NAME)
        nameSubTest = intent.getStringExtra(EditSubTestActivity.SUBTEST_NAME)

        rutaBD = intent.getStringExtra(RUTA_BD)

        btnCrear.setOnClickListener{
            saveItem()
        }

    }

    private fun saveItem(){
        val name = nombreItem.text.toString().trim()
        val type =
            if(tipoItem.isChecked) tipoItem.textOn.toString().trim() else tipoItem.textOff.toString().trim()


        if(name == ""){
            Toast.makeText(this, "El campo del nombre no puede ser vacío", Toast.LENGTH_SHORT).show()
            return
        }
        else{
            val ref = FirebaseDatabase.getInstance().getReference("tests/$rutaBD")
            val itemId = ref.push().key

            val nuevoItem = Item(itemId!!, name, type, TestsActivity.ITEM)

            ref.child(itemId).setValue(nuevoItem).addOnCompleteListener{
                //Toast.makeText(applicationContext, "¡Item guardado!", Toast.LENGTH_LONG).show()
            }

            val intent = Intent(this, EditSimpleItemActivity::class.java)
            intent.putExtra(RUTA_BD, "$rutaBD/$itemId/Bloques")
            intent.putExtra(EditSubTestActivity.SUBTEST_ID, idSubTest)
            intent.putExtra(EditTestActivity.TEST_ID, idTest)
            intent.putExtra(EditTestActivity.TEST_NAME, nameTest)
            intent.putExtra(EditSubTestActivity.SUBTEST_NAME, nameSubTest)
            intent.putExtra(TestsActivity.ITEM_ID, itemId)
            intent.putExtra(TestsActivity.ITEM_NAME, name)
            intent.putExtra(TestsActivity.ITEM_DESC, type)
            startActivity(intent)
            this.finish()
        }
    }

    companion object {
        const val RUTA_BD = "com.andes.chameleon.testfolders.RUTA_BD"
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE)
        return if (connectivityManager is ConnectivityManager) {
            val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
            networkInfo?.isConnected ?: false
        } else false
    }

    override fun onStart() {
        super.onStart()

        broadRec = object : BroadcastReceiver() {

            override fun onReceive(context: Context, intent: Intent) {
                if(!isNetworkAvailable())
                {
                    val snackbar = Snackbar.make(findViewById(R.id.itemActivity),"Sin conexión, los datos se actualizarán una vez vuelva la conexión", Snackbar.LENGTH_INDEFINITE)
                    snackbar.setAction("Aceptar") { snackbar.dismiss() }

                    snackbar.show()
                }
            }
        }

        registerReceiver(broadRec, IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"))
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(broadRec)
    }
}


