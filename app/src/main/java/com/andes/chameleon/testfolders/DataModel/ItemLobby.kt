package com.andes.chameleon.testfolders.DataModel

class ItemLobby (val orden: Int, val enunciado: String, val estado: String, val respondeExaminado: Boolean, val tipoScore: String,
                 val configuracionScore: HashMap<String, String>, val esUltimo: Boolean)