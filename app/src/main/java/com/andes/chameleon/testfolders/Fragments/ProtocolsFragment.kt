package com.andes.chameleon.testfolders.Fragments


import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import com.andes.chameleon.testfolders.ChoosePatientActivity
import com.andes.chameleon.testfolders.CreateProtocolActivty
import com.andes.chameleon.testfolders.DataModel.ProtocolPull
import com.andes.chameleon.testfolders.R
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.database.*

class ProtocolsFragment : Fragment() {

    private lateinit var protocolsRef : DatabaseReference
    private lateinit var protocolPopup : Dialog

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view : View = inflater.inflate(R.layout.fragment_protocols, container, false)
        protocolPopup = Dialog(activity)
        val btnCreateProtocol = view.findViewById<FloatingActionButton>(R.id.fabAddProtocol)
        val dataList: RecyclerView = view.findViewById(R.id.protocols_list)
        dataList.layoutManager = LinearLayoutManager(context)

        protocolsRef = FirebaseDatabase.getInstance().reference.child("protocolos")

        val option = FirebaseRecyclerOptions.Builder<ProtocolPull>()
            .setQuery(protocolsRef, ProtocolPull::class.java)
            .build()

        val nAdapter = object : FirebaseRecyclerAdapter<ProtocolPull, ProtocolsViewHolder>(option){
            override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ProtocolsViewHolder {
                val view1 : View = LayoutInflater.from(p0.context).inflate(R.layout.protocol_card_item, p0, false)

                return ProtocolsViewHolder(view1)
            }

            override fun onBindViewHolder(holder: ProtocolsViewHolder, position: Int, model: ProtocolPull) {

                val protocoloIds : String = getRef(position).key!!
                val listener = object : ValueEventListener {

                    var pName = ""
                    var pDesc = ""
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        pName = dataSnapshot.child("name").value.toString()
                        pDesc = dataSnapshot.child("description").value.toString()
                        holder.protocolName.text = pName
                        holder.protocolDesc.text = pDesc

                        holder.itemView.setOnClickListener{
                            showPopup(pName, pDesc, protocoloIds)
                        }
                    }


                    override fun onCancelled(databaseError: DatabaseError) {
                        println("loadPost:onCancelled ${databaseError.toException()}")
                    }
                }

                protocolsRef.child(protocoloIds).addValueEventListener(listener)
            }

        }
        dataList.adapter = nAdapter
        nAdapter.startListening()

        btnCreateProtocol.setOnClickListener{
            val intent = Intent (context, CreateProtocolActivty::class.java)
            startActivity(intent)
        }
        return view
    }

    private fun showPopup(name: String, description: String, protocolID: String){
        protocolPopup.setCancelable(false)
        protocolPopup.setContentView(R.layout.protocol_popup)
        val pName = protocolPopup.findViewById(R.id.popupName) as TextView
        val pDescription = protocolPopup.findViewById(R.id.popupDescription) as TextView

        pName.text = name
        pDescription.text = description

        val closeBtn = protocolPopup.findViewById(R.id.btnCerrarPopup) as TextView
        val applyBtn = protocolPopup.findViewById(R.id.btnAplicarPopup) as Button
        val deleteBtn = protocolPopup.findViewById(R.id.btnEliminarPopup) as ImageButton

        applyBtn.setOnClickListener {
            val choosePatientIntent = Intent (context, ChoosePatientActivity::class.java)
            choosePatientIntent.putExtra(PROTOCOL_ID, protocolID)
            choosePatientIntent.putExtra(PROTOCOL_NAME, name)
            protocolPopup.dismiss()
            startActivity(choosePatientIntent)
        }

        deleteBtn.setOnClickListener{
            val builder = AlertDialog.Builder(activity)

            // Set the alert dialog title
            builder.setTitle("Eliminar protocolo")

            // Display a message on alert dialog
            builder.setMessage("¿Está seguro de eliminar el protocolo?")

            // Set a positive button and its click listener on alert dialog
            builder.setPositiveButton("Si"){dialog, which ->
                // Do something when user press the positive button
                protocolsRef.child(protocolID).removeValue()
                protocolPopup.dismiss()
            }

            // Display a negative button on alert dialog
            builder.setNegativeButton("No"){dialog,which ->
            }

            // Finally, make the alert dialog using builder
            val dialog: AlertDialog = builder.create()

            // Display the alert dialog on app interface
            dialog.show()
        }

        closeBtn.setOnClickListener { protocolPopup.dismiss() }
        protocolPopup.window.setBackgroundDrawableResource(R.color.colorInvisible)
        protocolPopup.show()
    }

    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        const val PROTOCOL_ID = "com.andes.chameleon.testfolders.ID_PROTOCOL"
        const val PROTOCOL_NAME = "com.andes.chameleon.testfolders.NAME_PROTOCOL"
    }

    class ProtocolsViewHolder(itemView:View): RecyclerView.ViewHolder(itemView) {
        var protocolName : TextView = itemView.findViewById(R.id.protocol_card_name)
        var protocolDesc : TextView = itemView.findViewById(R.id.protocol_card_description)
    }
}

