package com.andes.chameleon.testfolders

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.widget.Button
import android.widget.EditText
import android.widget.Switch
import android.widget.Toast
import com.google.firebase.database.FirebaseDatabase

class EditMessageActivity : AppCompatActivity() {

    private lateinit var broadRec: BroadcastReceiver
    private lateinit var btnEditar: Button
    private lateinit var message: EditText
    private lateinit var visibilidad: Switch
    private lateinit var rutaBD: String
    private lateinit var mensajeId: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_message)

        btnEditar = findViewById(R.id.btnEditarMsg)
        message = findViewById(R.id.txtNuevoMensaje)
        visibilidad = findViewById(R.id.nuevaVisibilidadMsg)

        rutaBD = intent.getStringExtra(CreateItemActivity.RUTA_BD)
        mensajeId = intent.getStringExtra(TestsActivity.MENSAJE_ID)

        btnEditar.setOnClickListener {
            editarMensaje()
        }
    }

    private fun editarMensaje()
    {
        val msg = message.text.toString().trim()
        val visibility = visibilidad.isChecked

        when (msg) {
            "" -> {
                Toast.makeText(this, "El campo de mensaje no puede ser vacío", Toast.LENGTH_SHORT).show()
                return
            }
            else -> {

                val ref = FirebaseDatabase.getInstance().getReference("tests/$rutaBD")

                ref.child(mensajeId).child("mensaje").setValue(msg)
                ref.child(mensajeId).child("visibleExaminado").setValue(visibility)

                this.finish()
            }
        }
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE)
        return if (connectivityManager is ConnectivityManager) {
            val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
            networkInfo?.isConnected ?: false
        } else false
    }

    override fun onStart() {
        super.onStart()

        broadRec = object : BroadcastReceiver() {

            override fun onReceive(context: Context, intent: Intent) {
                if(!isNetworkAvailable())
                {
                    val snackbar = Snackbar.make(findViewById(R.id.editarMensaje),"Sin conexión, los datos se actualizarán una vez vuelva la conexión", Snackbar.LENGTH_INDEFINITE)
                    snackbar.setAction("Aceptar") { snackbar.dismiss() }

                    snackbar.show()
                }
            }
        }

        registerReceiver(broadRec, IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"))
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(broadRec)
    }
}
