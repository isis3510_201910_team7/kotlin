package com.andes.chameleon.testfolders.DataModel

import java.text.SimpleDateFormat
import java.util.*

class ResultadoProtocolo (val id: String, val idProtocolo: String, val idPaciente: String,
                          val fecha: String = SimpleDateFormat("dd/MM/yyyy").format(Date()))