package com.andes.chameleon.testfolders

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import com.andes.chameleon.testfolders.DataModel.InsideItemPull
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.github.clans.fab.FloatingActionButton
import com.google.firebase.database.*

class EditSimpleItemActivity : AppCompatActivity() {

    private lateinit var fabText: FloatingActionButton
    private lateinit var fabQualifTool: FloatingActionButton
    private lateinit var fabScore: FloatingActionButton

    private lateinit var btnEdit: ImageButton

    private lateinit var idTest: String
    private lateinit var idSubTest: String
    private lateinit var idItem: String

    private lateinit var nameTest: String
    private lateinit var nameSubTest: String
    private lateinit var nameItem: String
    private lateinit var descItem: String

    private lateinit var rutaBD: String
    private lateinit var itemsRef : DatabaseReference
    private lateinit var broadRec: BroadcastReceiver

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_simple_item)

        idTest = intent.getStringExtra(EditTestActivity.TEST_ID)
        idSubTest = intent.getStringExtra(EditSubTestActivity.SUBTEST_ID)
        idItem = intent.getStringExtra(TestsActivity.ITEM_ID)


        nameTest = intent.getStringExtra(EditTestActivity.TEST_NAME)
        nameSubTest = intent.getStringExtra(EditSubTestActivity.SUBTEST_NAME)
        nameItem = intent.getStringExtra(TestsActivity.ITEM_NAME)
        descItem = intent.getStringExtra(TestsActivity.ITEM_DESC)

        fabText = findViewById(R.id.fabCreateTextBlock)
        fabQualifTool = findViewById(R.id.fabCreateQualificationTool)
        fabScore = findViewById(R.id.fabCreateScore)

        btnEdit = findViewById<ImageButton>(R.id.editItem)

        val dataList: RecyclerView = findViewById(R.id.inside_items_list)
        dataList.layoutManager = LinearLayoutManager(this)

        val currentItemName = findViewById<TextView>(R.id.itemName)
        val currentSubTestName = findViewById<TextView>(R.id.subtestName)
        val currentTestName = findViewById<TextView>(R.id.testName)
        val currentItemName2 = findViewById<TextView>(R.id.itemName2)
        val currentItemDesc = findViewById<TextView>(R.id.itemDescription)

        currentItemName.text = nameItem
        currentSubTestName.text = nameSubTest
        currentTestName.text = nameTest
        currentItemName2.text = nameItem
        currentItemDesc.text = descItem

        currentSubTestName.setOnClickListener{
            this.finish()
        }

        btnEdit.setOnClickListener{
            val intent = Intent (applicationContext, EditItemActivity::class.java)
            intent.putExtra(TestsActivity.ITEM_ID, idItem)
            intent.putExtra(CreateItemActivity.RUTA_BD, "$idTest/Bloques/$idSubTest/Bloques")
            startActivity(intent)
        }

        rutaBD = intent.getStringExtra(CreateItemActivity.RUTA_BD)

        itemsRef = FirebaseDatabase.getInstance().reference.child("tests").child(idTest).child("Bloques").child(idSubTest).child("Bloques").child(idItem).child("Bloques")


        val option = FirebaseRecyclerOptions.Builder<InsideItemPull>()
            .setQuery(itemsRef, InsideItemPull::class.java)
            .build()

        val nAdapter = object : FirebaseRecyclerAdapter<InsideItemPull, InsideItemViewHolder>(option){
            override fun onCreateViewHolder(p0: ViewGroup, p1: Int): InsideItemViewHolder {
                val view : View = LayoutInflater.from(p0.context).inflate(R.layout.inside_item_card_item, p0, false)

                return InsideItemViewHolder(view)
            }

            override fun onBindViewHolder(holder: InsideItemViewHolder, position: Int, model: InsideItemPull) {

                val insideItemIds: String = getRef(position).key!!
                Log.d("Item id", insideItemIds)

                var iID :String
                var iType: String

                val listener = object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        if(dataSnapshot.exists()){

                            if (dataSnapshot.hasChild("id")){
                                iID = dataSnapshot.child("id").value.toString()
                                iType = dataSnapshot.child("tipo").value.toString()
                                holder.insideItemID.text = iID
                                holder.insideItemType.text = iType
                            }
                        }
                    }

                    override fun onCancelled(databaseError: DatabaseError) {
                        println("loadPost:onCancelled ${databaseError.toException()}")
                    }
                }

                itemsRef.child(insideItemIds).addValueEventListener(listener)
            }
        }
        dataList.adapter = nAdapter
        nAdapter.startListening()

        fabText.setOnClickListener {
            val intent = Intent (this, CreateTextBlockActivity::class.java)
            intent.putExtra(CreateItemActivity.RUTA_BD, rutaBD)
            startActivity(intent)
        }

        fabQualifTool.setOnClickListener {
            val intent = Intent (this, CreateQualificationToolActivity::class.java)
            intent.putExtra(CreateItemActivity.RUTA_BD, rutaBD)
            startActivity(intent)
        }

        fabScore.setOnClickListener {
            val intent = Intent (this, CreateScoreActivity::class.java)
            intent.putExtra(CreateItemActivity.RUTA_BD, rutaBD)
            startActivity(intent)
        }
    }

    class InsideItemViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        var insideItemID : TextView = itemView.findViewById(R.id.inside_item_card_id)
        var insideItemType : TextView = itemView.findViewById(R.id.inside_item_card_type)
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE)
        return if (connectivityManager is ConnectivityManager) {
            val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
            networkInfo?.isConnected ?: false
        } else false
    }

    override fun onStart() {
        super.onStart()

        broadRec = object : BroadcastReceiver() {

            override fun onReceive(context: Context, intent: Intent) {
                if(!isNetworkAvailable())
                {
                    val snackbar = Snackbar.make(findViewById(R.id.editSimpleItemView),"Sin conexión, los datos se actualizarán una vez vuelva la conexión", Snackbar.LENGTH_INDEFINITE)
                    snackbar.setAction("Aceptar") { snackbar.dismiss() }

                    snackbar.show()
                }
            }
        }

        registerReceiver(broadRec, IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"))
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(broadRec)
    }
}
