package com.andes.chameleon.testfolders

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.view.Menu
import com.andes.chameleon.testfolders.Fragments.ExaminedFragment
import com.andes.chameleon.testfolders.Fragments.ProtocolsFragment
import com.andes.chameleon.testfolders.Fragments.TestsFragment
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_tests.*
import kotlinx.android.synthetic.main.appbar_tests.*

class TestsActivity : AppCompatActivity(), ExaminedFragment.OnFragmentInteractionListener, TestsFragment.OnFragmentInteractionListener, ProtocolsFragment.OnFragmentInteractionListener{

    private var pagerAdapter :CustomPagerAdapter ?= null
    private lateinit var broadRec: BroadcastReceiver
    private var cacheFirebase: Boolean = false

    override fun onFragmentInteraction(uri: Uri) {
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tests)

        
        if (savedInstanceState != null) {
            cacheFirebase = savedInstanceState.getBoolean("FIREBASE")
        }

        if(!cacheFirebase)
        {
            FirebaseDatabase.getInstance().setPersistenceEnabled(true)
            cacheFirebase = true
        }

        setSupportActionBar(toolbar_tests)
        pagerAdapter = CustomPagerAdapter(supportFragmentManager)
        pagerAdapter!!.addFragments(ProtocolsFragment(),"Protocolos")
        pagerAdapter!!.addFragments(TestsFragment(), "Pruebas")
        pagerAdapter!!.addFragments(ExaminedFragment(), "Examinados")

        customViewPager.adapter = pagerAdapter

        customTabLayout.setupWithViewPager(customViewPager)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        outState?.putBoolean("FIREBASE", cacheFirebase)
        super.onSaveInstanceState(outState)
    }

    override fun onStart() {
        super.onStart()

        broadRec = object : BroadcastReceiver() {

            override fun onReceive(context: Context, intent: Intent) {
                if(!isNetworkAvailable())
                {
                    val snackbar = Snackbar.make(findViewById(R.id.mainLayout),"Sin conexión, los datos se actualizarán una vez vuelva la conexión", Snackbar.LENGTH_INDEFINITE)
                    snackbar.setAction("Aceptar") { snackbar.dismiss() }

                    snackbar.show()
                }
            }
        }

        registerReceiver(broadRec, IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"))
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(broadRec)
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE)
        return if (connectivityManager is ConnectivityManager) {
            val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
            networkInfo?.isConnected ?: false
        } else false
    }

    companion object {
        const val SUBTEST = "SUBTEST"
        const val TEST = "TEST"
        const val MENSAJE = "MENSAJE"
        const val NOMBRE_MENSAJE = "NOMBRE_MENSAJE"
        const val ITEM = "ITEM"
        const val TEXTO = "TEXTO"
        const val QUALIFICATION_TOOL = "QUALIFICATION_TOOL"
        const val SCORE = "SCORE"
        const val CONFIGURACION = "CONFIGURACION"
        const val PROTOCOLO_ID = "PROTOCOLO_ID"
        const val ITEM_ID = "ITEM_ID"
        const val ITEM_NAME = "ITEM_NAME"
        const val ITEM_DESC = "ITEM_DESC"
        const val MENSAJE_ID = "MENSAJE_ID"
        const val VISIBLE_EXAMINADO = "VISIBLE_EXAMINADO"
        const val LOBBY = "LOBBY"
        const val ORDEN_ITEM = "ORDEN_ITEM"
    }

}
