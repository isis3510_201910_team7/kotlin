package com.andes.chameleon.testfolders.DataModel

class ItemApply(tipoApl: String, var item: Item): Aplicable(tipoApl) {
    lateinit var enunciado: TextModel
    lateinit var tool: QualificationTool
    lateinit var score: Score
    lateinit var idProtocolo: String
    lateinit var idTest: String
    lateinit var idSubTest: String
}