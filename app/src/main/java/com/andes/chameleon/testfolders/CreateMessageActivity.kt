package com.andes.chameleon.testfolders


import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.widget.Button
import android.widget.EditText
import android.widget.Switch
import android.widget.Toast
import com.andes.chameleon.testfolders.DataModel.Mensaje
import com.google.firebase.database.FirebaseDatabase

class CreateMessageActivity : AppCompatActivity() {

    private lateinit var broadRec: BroadcastReceiver

    private lateinit var btnCrear: Button
    private lateinit var message: EditText
    private lateinit var visibilidad: Switch
    private lateinit var nombreMensaje: EditText

    private lateinit var rutaBD: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_message)

        btnCrear = findViewById(R.id.btnCrearMensaje)
        message = findViewById(R.id.txtMessage)
        visibilidad = findViewById(R.id.swVisibilidadMensaje)
        nombreMensaje = findViewById(R.id.txtNombreMssg)

        rutaBD = intent.getStringExtra(CreateItemActivity.RUTA_BD)

        btnCrear.setOnClickListener {
            saveMessage()
        }

    }

    private fun saveMessage()
    {
        val name = nombreMensaje.text.toString().trim()
        val msg = message.text.toString().trim()
        val visibility = visibilidad.isChecked

        when {
            name == "" -> {
                Toast.makeText(this, "El campo del nombre no puede ser vacío", Toast.LENGTH_SHORT).show()
                return
            }
            msg == "" -> {
                Toast.makeText(this, "El campo de mensaje no puede ser vacío", Toast.LENGTH_SHORT).show()
                return
            }
            else -> {
                val ref = FirebaseDatabase.getInstance().getReference("tests/$rutaBD")
                val messageId = ref.push().key

                val nuevoMsg = Mensaje(messageId!!, name , msg, visibility, TestsActivity.MENSAJE)

                ref.child(messageId).setValue(nuevoMsg).addOnCompleteListener{
                    //Toast.makeText(applicationContext, "¡Mensaje guardado!", Toast.LENGTH_LONG).show()
                }

                this.finish()
            }
        }
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE)
        return if (connectivityManager is ConnectivityManager) {
            val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
            networkInfo?.isConnected ?: false
        } else false
    }

    override fun onStart() {
        super.onStart()

        broadRec = object : BroadcastReceiver() {

            override fun onReceive(context: Context, intent: Intent) {
                if(!isNetworkAvailable())
                {
                    val snackbar = Snackbar.make(findViewById(R.id.messageActivity),"Sin conexión, los datos se actualizarán una vez vuelva la conexión", Snackbar.LENGTH_INDEFINITE)
                    snackbar.setAction("Aceptar") { snackbar.dismiss() }

                    snackbar.show()
                }
            }
        }

        registerReceiver(broadRec, IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"))
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(broadRec)
    }
}
