package com.andes.chameleon.testfolders

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

class CustomPagerAdapter(fm:FragmentManager) : FragmentPagerAdapter(fm) {

    private var mFragmentItems: ArrayList<Fragment> = ArrayList()
    private var mFragmentTitles: ArrayList<String> = ArrayList()

    fun addFragments(fragmentItem : Fragment, fragmentTitle: String){
        mFragmentItems.add(fragmentItem)
        mFragmentTitles.add(fragmentTitle)
    }

    override fun getItem(p0: Int): Fragment {
        return mFragmentItems[p0]
        //FragmentTests()

    }

    override fun getCount(): Int {
        return mFragmentItems.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return mFragmentTitles[position]
    }
}