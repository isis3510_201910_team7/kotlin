package com.andes.chameleon.testfolders

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Bitmap
import android.media.MediaPlayer
import android.media.MediaRecorder
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.os.SystemClock
import android.support.design.widget.Snackbar
import android.support.v7.widget.CardView
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import com.andes.chameleon.testfolders.DataModel.ResultadoItem
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import java.io.*
import java.util.*

class ApplyActivity : AppCompatActivity() {

    private lateinit var broadRec: BroadcastReceiver
    private var estadoAnterior = false
    private val cameraRequest = 1
    private val externalStorage = Environment.getExternalStorageDirectory().absoluteFile
    private lateinit var storageRef: StorageReference


    private lateinit var enunciado: String
    private lateinit var toolCalificacion: String
    private lateinit var tipoScore: String
    private lateinit var txtEnunciado: TextView
    private lateinit var btnSiguiente: Button
    private lateinit var configuracion: Array<String>

    private lateinit var nombreItem: String
    private lateinit var rutaBD: String
    private lateinit var idTest: String
    private lateinit var idSubTest: String
    private lateinit var idItem: String
    private var resultadoEscogido = false

    private lateinit var resultadoTool: String
    private lateinit var resultadoScore: String

    //Grabar voz
    private lateinit var btnStart: Button
    private lateinit var btnStop: Button
    private lateinit var btnPlay: Button
    private lateinit var btnGuardarVoz: Button
    private var recorder: MediaRecorder ?= null
    private lateinit var recordingFileName: String
    private lateinit var randUUIDGrabar: UUID

    //Tomar foto
    private lateinit var btnFoto: Button
    private lateinit var btnGuardarFoto: Button
    private lateinit var imgTomada: ImageView
    private lateinit var randUUIDFoto: UUID
    private lateinit var fotoFileName: String
    private lateinit var fotoBitmap: Bitmap

    //Tomar tiempo
    private lateinit var cronometro: Chronometer
    private lateinit var btnCronometro: Button
    private lateinit var btnGuardarTime: Button
    private var isRunning = false

    //Comentarios
    private lateinit var comentarios: EditText
    private lateinit var btnGuardarCom: Button

    private var respondeExaminado = false
    private lateinit var idLobby: String
    private var ordenItem = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_apply)

        storageRef = FirebaseStorage.getInstance().reference

        rutaBD = intent.getStringExtra(CreateItemActivity.RUTA_BD)
        idTest = intent.getStringExtra(EditTestActivity.TEST_ID)
        nombreItem = intent.getStringExtra(TestsActivity.ITEM_NAME)
        idSubTest = intent.getStringExtra(EditSubTestActivity.SUBTEST_ID)
        idItem = intent.getStringExtra(TestsActivity.ITEM_ID)

        enunciado = intent.getStringExtra(TestsActivity.TEXTO)
        toolCalificacion = intent.getStringExtra(TestsActivity.QUALIFICATION_TOOL)
        tipoScore = intent.getStringExtra(TestsActivity.SCORE)
        configuracion = intent.getStringArrayExtra(TestsActivity.CONFIGURACION)
        respondeExaminado = intent.getBooleanExtra(TestsActivity.VISIBLE_EXAMINADO, false)
        idLobby = intent.getStringExtra(TestsActivity.LOBBY)
        ordenItem = intent.getIntExtra(TestsActivity.ORDEN_ITEM, 0)

        btnSiguiente = findViewById(R.id.btnNextItem)

        btnSiguiente.setOnClickListener {
            siguienteItem()
        }

        btnSiguiente.isEnabled = false

        this.title = nombreItem

        when(toolCalificacion)
        {
            getString(R.string.GrabarVoz) -> {
                btnStart = findViewById(R.id.btnStartRecording)
                btnStop = findViewById(R.id.btnStopRecord)
                btnPlay = findViewById(R.id.btnEscuchar)
                btnGuardarVoz = findViewById(R.id.btnGuardarRecord)

                btnPlay.isEnabled = false
                btnStop.isEnabled = false
                btnGuardarVoz.isEnabled = false

                randUUIDGrabar = UUID.randomUUID()

                recordingFileName = "$externalStorage/$randUUIDGrabar.3gp"

                btnStart.setOnClickListener {
                    startRecording(recordingFileName)
                    btnStart.isEnabled = false
                    btnStop.isEnabled = true
                }

                btnStop.setOnClickListener {
                    stopRecording()

                    btnStop.isEnabled = false
                    btnPlay.isEnabled = true
                    btnGuardarVoz.isEnabled = true
                }

                btnPlay.setOnClickListener {
                    val mediaPlayer = MediaPlayer.create (this, Uri.fromFile(File(recordingFileName)))
                    mediaPlayer.start ()
                }

                btnGuardarVoz.setOnClickListener {
                    saveRecord()
                }

                findViewById<CardView>(R.id.CardGrabarVoz).visibility = View.VISIBLE
            }

            getString(R.string.TomarFoto) -> {
                btnFoto = findViewById(R.id.btnTomarFoto)
                btnGuardarFoto = findViewById(R.id.btnGuardarFoto)
                imgTomada = findViewById(R.id.imgFoto)

                btnGuardarFoto.isEnabled = false

                randUUIDFoto = UUID.randomUUID()

                fotoFileName = "$externalStorage/$randUUIDFoto.jpg"

                btnFoto.setOnClickListener {

                    val intent = Intent("android.media.action.IMAGE_CAPTURE")
                    startActivityForResult(intent, cameraRequest)

                }

                btnGuardarFoto.setOnClickListener {
                    saveImage(fotoBitmap)
                    btnGuardarFoto.isEnabled = false
                }

                findViewById<CardView>(R.id.CardFoto).visibility = View.VISIBLE
            }

            getString(R.string.TomarTiempo) -> {
                cronometro = findViewById(R.id.timer)
                btnCronometro = findViewById(R.id.btnStartTimer)
                btnGuardarTime = findViewById(R.id.btnGuardarTiempo)

                btnGuardarTime.isEnabled = false

                var timeWhenStopped = 0L

                btnCronometro.setOnClickListener {

                    if(!isRunning)
                    {
                        cronometro.base = SystemClock.elapsedRealtime() + timeWhenStopped
                        cronometro.start()
                        isRunning = true
                        btnGuardarTime.isEnabled = false
                    }
                    else
                    {
                        timeWhenStopped = cronometro.base - SystemClock.elapsedRealtime()
                        cronometro.stop()
                        isRunning = false
                        btnGuardarTime.isEnabled = true
                    }

                    btnCronometro.text = if(isRunning) getString(R.string.stop) else getString(R.string.start)
                }


                btnGuardarTime.setOnClickListener {
                    //Toast.makeText(applicationContext, "¡Tiempo guardado: ${cronometro.text}", Toast.LENGTH_LONG).show()
                    btnGuardarTime.isEnabled = false

                    resultadoTool = cronometro.text.toString()
                    btnSiguiente.isEnabled = true
                }

                findViewById<CardView>(R.id.cardTimer).visibility = View.VISIBLE
            }

            getString(R.string.AreaTexto) -> {
                comentarios = findViewById(R.id.txtComentarios)
                btnGuardarCom = findViewById(R.id.btnGuardarComentarios)

                btnGuardarCom.setOnClickListener {
                    resultadoTool = comentarios.text.toString().trim()
                    //Toast.makeText(applicationContext, "¡Comentario guardado!", Toast.LENGTH_LONG).show()
                    btnGuardarCom.isEnabled = false
                    btnSiguiente.isEnabled = true
                }

                findViewById<CardView>(R.id.CardComentarios).visibility = View.VISIBLE
            }
        }

        txtEnunciado = findViewById(R.id.txtEnunciadoItem)
        txtEnunciado.text = enunciado

        val inflater: LayoutInflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.radio_group_layout, null)
        val parentLinearLayout = findViewById<LinearLayout>(R.id.parent_radio_group)
        parentLinearLayout.addView(view, parentLinearLayout.childCount - 1)


        val layout = findViewById<LinearLayout>(R.id.RadioGroupLayout)
        val radioGroupScore = RadioGroup(this)

        radioGroupScore.orientation = RadioGroup.VERTICAL

        var radioBtn: RadioButton
        var radioParams: RadioGroup.LayoutParams

        when(tipoScore)
        {
            getString(R.string.Numerico) ->
            {
                var i = configuracion[0].toDouble()
                val fin = configuracion[2].toDouble()

                while(i <= fin) {
                    radioBtn = RadioButton(this)
                    radioBtn.text = "$i"

                    radioParams =
                        RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.MATCH_PARENT)

                    radioGroupScore.addView(radioBtn, radioParams)

                    i += configuracion[1].toDouble()
                }
            }

            getString(R.string.Textual) ->
            {
                for (i in 0 until configuracion.size) {
                    radioBtn = RadioButton(this)
                    radioBtn.text = configuracion[i]

                    radioParams =
                        RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.MATCH_PARENT)

                    radioGroupScore.addView(radioBtn, radioParams)
                }
            }

        }

        if(respondeExaminado)
        {
            for(i in 0 until radioGroupScore.childCount)
            {
                radioGroupScore.getChildAt(i).isEnabled = false
            }

            resultadoScore = "A"
            resultadoEscogido = true
        }

        radioGroupScore.setOnCheckedChangeListener { _, checkedId ->
            resultadoScore = findViewById<RadioButton>(checkedId).text.toString()
            resultadoEscogido = true
        }

        layout.addView(radioGroupScore)

        /*val ref = FirebaseDatabase.getInstance().getReference("Aaa").child("$idLobby")

        ref.addValueEventListener(object : ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {

            }

            override fun onDataChange(p0: DataSnapshot) {
                Toast.makeText(applicationContext, "Cambio", Toast.LENGTH_LONG).show()
            }

        })
*/
    }

    private fun siguienteItem()
    {
        if(!resultadoEscogido)
        {
            Toast.makeText(applicationContext, "Debe seleccionar un puntaje", Toast.LENGTH_LONG).show()
            return
        }

        if(respondeExaminado)
        {
            val ref = FirebaseDatabase.getInstance().getReference("lobbies").child(idLobby).child("items").child("it$ordenItem")
            val resItemId = FirebaseDatabase.getInstance().getReference("$rutaBD/Bloques").push().key

            ref.child("resItemId").setValue(resItemId!!)
            ref.child("idTest").setValue(idTest)
            ref.child("idSubTest").setValue(idSubTest)
            ref.child("idItem").setValue(idItem)
            ref.child("resultadoTool").setValue(resultadoTool)
            ref.child("resultadoScore").setValue(resultadoScore)
            ref.child("rutaBD").setValue(rutaBD)

        }
        else
        {
            val ref = FirebaseDatabase.getInstance().getReference("$rutaBD/Bloques")
            val resItemId = ref.push().key

            val nuevoResultado = ResultadoItem(resItemId!!, idTest, idSubTest, idItem, resultadoTool, resultadoScore)

            ref.child(resItemId).setValue(nuevoResultado).addOnCompleteListener{

            }
        }

        setResult(Activity.RESULT_OK)



        finish()
    }

    private fun startRecording(recordingFileName: String) {
        recorder = MediaRecorder().apply {
            setAudioSource(MediaRecorder.AudioSource.MIC)
            setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP)
            setOutputFile(recordingFileName)
            setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB)

            try {
                prepare()
            } catch (e: IOException) {

                //Log.e(LOG_TAG, "prepare() failed")
                e.printStackTrace()
            }

            start()
        }
    }

    private fun stopRecording() {
        recorder?.apply {
            stop()
            release()
        }
        recorder = null
    }

    private fun saveRecord()
    {
        resultadoTool = "recordings/$randUUIDGrabar"

        val uri: Uri = Uri.fromFile(File(recordingFileName))
        val path: StorageReference? = storageRef.child("recordings/$randUUIDGrabar")
        path!!.putFile(uri).addOnSuccessListener {
            //Toast.makeText(applicationContext, "¡Audio guardado!", Toast.LENGTH_LONG).show()
        }.addOnFailureListener{
            it.printStackTrace()
        }

        btnGuardarVoz.isEnabled = false
        btnSiguiente.isEnabled = true
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == cameraRequest && resultCode == Activity.RESULT_OK) {

            fotoBitmap = data!!.extras.get("data") as Bitmap

            imgTomada.setImageBitmap(fotoBitmap)
            saveImageInternal(fotoBitmap)

            btnGuardarFoto.isEnabled = true
        }

    }

    private fun saveImage(img: Bitmap)
    {
        resultadoTool = "images/$randUUIDFoto"

        val imagesRef = storageRef.child("images/$randUUIDFoto")

        val baos = ByteArrayOutputStream()
        img.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val data = baos.toByteArray()

        val uploadTask = imagesRef.putBytes(data)
        uploadTask.addOnFailureListener {
            // Handle unsuccessful uploads
            it.printStackTrace()
        }.addOnSuccessListener {
            // taskSnapshot.metadata contains file metadata such as size, content-type, etc.
            // ...
            //Toast.makeText(applicationContext, "¡Imagen guardada!", Toast.LENGTH_LONG).show()
        }

        btnSiguiente.isEnabled = true
    }

    private fun saveImageInternal(bitmap: Bitmap)
    {

        val file = File(externalStorage, "$randUUIDFoto.jpg")

        try {
            // Get the file output stream
            val stream: OutputStream = FileOutputStream(file)

            // Compress the bitmap
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream)

            // Flush the output stream
            stream.flush()

            // Close the output stream
            stream.close()

        } catch (e: IOException){ // Catch the exception

            e.printStackTrace()
        }

    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE)
        return if (connectivityManager is ConnectivityManager) {
            val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
            networkInfo?.isConnected ?: false
        } else false
    }

    override fun onStart() {
        super.onStart()

        broadRec = object : BroadcastReceiver() {

            override fun onReceive(context: Context, intent: Intent) {
                /*if(!isNetworkAvailable())
                {
                    val snackbar = Snackbar.make(findViewById(R.id.applylayout),"Sin conexión, los datos se actualizarán una vez vuelva la conexión", Snackbar.LENGTH_INDEFINITE)
                    snackbar.setAction("Aceptar") { snackbar.dismiss() }

                    snackbar.show()
                    estadoAnterior = btnSiguiente.isEnabled
                    btnSiguiente.isEnabled = false
                }
                else
                {
                    btnSiguiente.isEnabled = estadoAnterior
                }*/
            }
        }

        registerReceiver(broadRec, IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"))
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(broadRec)
    }
}