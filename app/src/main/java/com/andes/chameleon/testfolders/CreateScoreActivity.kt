package com.andes.chameleon.testfolders

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.CardView
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import com.andes.chameleon.testfolders.DataModel.Score
import com.google.firebase.database.FirebaseDatabase

class CreateScoreActivity : AppCompatActivity() {

    private lateinit var rutaBD: String
    private val maxSize = 20

    private lateinit var broadRec: BroadcastReceiver
    private lateinit var tipoScore: RadioGroup
    private lateinit var visibilidad: Switch
    private lateinit var configNumerico: CardView
    private lateinit var configTextual: CardView
    private lateinit var btnCrear: Button
    private lateinit var btnAgregarCampo: Button

    private lateinit var inicial: EditText
    private lateinit var jump: EditText
    private lateinit var end: EditText

    private lateinit var parentLinearLayout: LinearLayout
    private lateinit var lista: ArrayList<EditText>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_score)

        rutaBD = intent.getStringExtra(CreateItemActivity.RUTA_BD)

        tipoScore = findViewById(R.id.radioGroup)
        visibilidad = findViewById(R.id.swVisibilidadScore)
        btnCrear = findViewById(R.id.btnCrearScore)
        btnAgregarCampo = findViewById(R.id.btnAddField)
        configNumerico = findViewById(R.id.cardView2)
        configTextual = findViewById(R.id.cardTextualOptions)

        inicial = findViewById(R.id.txtInicial)
        jump = findViewById(R.id.txtJump)
        end = findViewById(R.id.txtEnd)

        btnCrear.setOnClickListener{

            if(verificarCampos())
            {
                saveScore()
            }

        }

        tipoScore.setOnCheckedChangeListener { _, _ ->
            switchVisibility()
        }

        lista = ArrayList()

        parentLinearLayout = findViewById(R.id.parent_linear)

        btnAgregarCampo.setOnClickListener{
            addField()
        }

        addField()

    }

    private fun addField()
    {
        val inflater: LayoutInflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val rowView = inflater.inflate(R.layout.field, null)

        parentLinearLayout.addView(rowView, parentLinearLayout.childCount - 1)
        lista.add(rowView.findViewById(R.id.number_edit_text))

        if (lista.size >= maxSize) btnAgregarCampo.isEnabled = false

    }

    fun onDelete(v: View)
    {
        val pView = v.parent as View
        lista.remove(pView.findViewById(R.id.number_edit_text))
        parentLinearLayout.removeView(pView)

        if(!btnAgregarCampo.isEnabled) btnAgregarCampo.isEnabled = true
    }

    private fun switchVisibility()
    {
        val temp = configNumerico.visibility
        configNumerico.visibility = configTextual.visibility
        configTextual.visibility = temp
    }

    private fun saveScore(){

        val tipo = findViewById<RadioButton>(tipoScore.checkedRadioButtonId).text.toString().trim()
        val visibility = visibilidad.isChecked
        val config = HashMap<String, String>()

        if(configNumerico.visibility == View.VISIBLE)
        {
            config["Inicial"] = inicial.text.toString().trim()
            config["Salto"] = jump.text.toString().trim()
            config["Fin"] = end.text.toString().trim()
        }
        else
        {
            for ((i, item: EditText) in lista.withIndex())
            {
                config["Opcion$i"] = item.text.toString().trim()
            }
        }

        val ref = FirebaseDatabase.getInstance().getReference("tests/$rutaBD")
        val scoreId = ref.push().key

        val nuevoScore = Score(scoreId!!, tipo, visibility, config, TestsActivity.SCORE)

        ref.child(scoreId).setValue(nuevoScore).addOnCompleteListener{
            //Toast.makeText(applicationContext, "¡Score guardado!", Toast.LENGTH_LONG).show()
        }

        finish()
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE)
        return if (connectivityManager is ConnectivityManager) {
            val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
            networkInfo?.isConnected ?: false
        } else false
    }

    override fun onStart() {
        super.onStart()

        broadRec = object : BroadcastReceiver() {

            override fun onReceive(context: Context, intent: Intent) {
                if(!isNetworkAvailable())
                {
                    val snackbar = Snackbar.make(findViewById(R.id.scoreActivity),"Sin conexión, los datos se actualizarán una vez vuelva la conexión", Snackbar.LENGTH_INDEFINITE)
                    snackbar.setAction("Aceptar") { snackbar.dismiss() }

                    snackbar.show()
                }
            }
        }

        registerReceiver(broadRec, IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"))
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(broadRec)
    }

    private fun verificarCampos(): Boolean{

        if(configNumerico.visibility == View.VISIBLE)
        {
            when {
                inicial.text.toString() == "" -> {
                    Toast.makeText(applicationContext, "El campo inicial no puede estar vacío", Toast.LENGTH_LONG).show()
                    return false
                }
                jump.text.toString() == "" -> {
                    Toast.makeText(applicationContext, "El campo salto no puede estar vacío", Toast.LENGTH_LONG).show()
                    return false
                }
                end.text.toString() == "" -> {
                    Toast.makeText(applicationContext, "El campo fin no puede estar vacío", Toast.LENGTH_LONG).show()
                    return false
                }

                else -> {
                    val valInit = inicial.text.toString().toDouble()
                    val valJump = jump.text.toString().toDouble()
                    val valEnd = end.text.toString().toDouble()

                    if (valInit >= valEnd) {
                        Toast.makeText(applicationContext, "El valor inical debe ser menor que el final", Toast.LENGTH_LONG).show()
                        return false
                    }

                    if((valEnd - valInit) % valJump != 0.0)
                    {
                        Toast.makeText(applicationContext, "El valor de salto no es coherente con el valor final", Toast.LENGTH_LONG).show()
                        return false
                    }
                }
            }

        }

        else
        {
            if(lista.size < 2)
            {
                Toast.makeText(applicationContext, "Debe haber por lo menos dos opciones", Toast.LENGTH_LONG).show()
                return false
            }

            if(hayRepetidos())
            {
                Toast.makeText(applicationContext, "No puede haber dos o más opciones repetidas", Toast.LENGTH_LONG).show()
                return false
            }

            for (item: EditText in lista)
            {
                if(item.text.toString() == "")
                {
                    Toast.makeText(applicationContext, "No puede haber opciones vacías", Toast.LENGTH_LONG).show()
                    return false
                }
            }
        }

        return true
    }

    private fun hayRepetidos(): Boolean
    {
        var actual: String

        for(i in 0 until lista.size - 1)
        {
            actual = lista[i].text.toString().toLowerCase().trim()

            for(j in i + 1 until lista.size)
            {
                if(actual == lista[j].text.toString().toLowerCase().trim()) return true
            }
        }

        return false
    }
}
