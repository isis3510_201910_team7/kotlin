package com.andes.chameleon.testfolders

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.widget.Button
import android.widget.EditText
import android.widget.Switch
import android.widget.Toast
import com.google.firebase.database.FirebaseDatabase

class EditItemActivity : AppCompatActivity() {

    private lateinit var rutaBD: String
    private lateinit var btnEditar: Button
    private lateinit var tipoItem: Switch
    private lateinit var nombreItem: EditText
    private lateinit var idItem: String
    private lateinit var broadRec: BroadcastReceiver

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_item)

        btnEditar = findViewById(R.id.btnEditarItem)
        tipoItem = findViewById(R.id.swTipoItem)
        nombreItem = findViewById(R.id.txtNombreItem)

        idItem = intent.getStringExtra(TestsActivity.ITEM_ID)
        rutaBD = intent.getStringExtra(CreateItemActivity.RUTA_BD)

        btnEditar.setOnClickListener{
            saveItem()
        }

    }

    private fun saveItem(){
        val name = nombreItem.text.toString().trim()
        val type =
            if(tipoItem.isChecked) tipoItem.textOn.toString().trim() else tipoItem.textOff.toString().trim()


        if(name == ""){
            Toast.makeText(this, "El campo del nombre no puede ser vacío", Toast.LENGTH_SHORT).show()
            return
        }
        else{
            val ref = FirebaseDatabase.getInstance().getReference("tests/$rutaBD")


            ref.child(idItem).child("nombre").setValue(name).addOnCompleteListener{

            }

            ref.child(idItem).child("simpleComplejo").setValue(type).addOnCompleteListener{

            }

            this.finish()
        }
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE)
        return if (connectivityManager is ConnectivityManager) {
            val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
            networkInfo?.isConnected ?: false
        } else false
    }

    override fun onStart() {
        super.onStart()

        broadRec = object : BroadcastReceiver() {

            override fun onReceive(context: Context, intent: Intent) {
                if(!isNetworkAvailable())
                {
                    val snackbar = Snackbar.make(findViewById(R.id.editItemAct),"Sin conexión, los datos se actualizarán una vez vuelva la conexión", Snackbar.LENGTH_INDEFINITE)
                    snackbar.setAction("Aceptar") { snackbar.dismiss() }

                    snackbar.show()
                }
            }
        }

        registerReceiver(broadRec, IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"))
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(broadRec)
    }
}
