package com.andes.chameleon.testfolders

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.andes.chameleon.testfolders.DataModel.SubTestPull
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.github.clans.fab.FloatingActionButton
import com.github.clans.fab.FloatingActionMenu
import com.google.firebase.database.*

class EditTestActivity : AppCompatActivity() {

    private lateinit var floatingMenu: FloatingActionMenu
    private lateinit var fabMessage: FloatingActionButton
    private lateinit var fabSubTest: FloatingActionButton
    private lateinit var fabSimpleItem: FloatingActionButton
    private var idTest: String = ""
    private var nameTest: String = ""
    private var descriptionTest: String = ""
    private lateinit var subTestsRef : DatabaseReference
    private lateinit var broadRec: BroadcastReceiver

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_test)

        idTest = intent.getStringExtra(TEST_ID)
        nameTest = intent.getStringExtra(TEST_NAME)
        descriptionTest = intent.getStringExtra(TEST_DESCRIPTION)

        val testName = findViewById<TextView>(R.id.testName)
        val testName2 = findViewById<TextView>(R.id.testName2)
        val testDesc = findViewById<TextView>(R.id.testDescription)
        testName.text = nameTest
        testName2.text = nameTest
        testDesc.text = descriptionTest

        val dataList: RecyclerView = findViewById(R.id.subtest_list)
        dataList.layoutManager = LinearLayoutManager(this)

        subTestsRef = FirebaseDatabase.getInstance().reference.child("tests").child(idTest).child("Bloques")

        val option = FirebaseRecyclerOptions.Builder<SubTestPull>()
            .setQuery(subTestsRef, SubTestPull::class.java)
            .build()

        val nAdapter = object : FirebaseRecyclerAdapter<SubTestPull, SubTestsViewHolder>(option){
            override fun onCreateViewHolder(p0: ViewGroup, p1: Int): SubTestsViewHolder {
                val view : View = LayoutInflater.from(p0.context).inflate(R.layout.subtest_card_item, p0, false)

                return SubTestsViewHolder(view)
            }

            override fun onBindViewHolder(holder: SubTestsViewHolder, position: Int, model: SubTestPull) {

                val subtestIds: String = getRef(position).key!!

                val listener = object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        var tName = ""
                        var tDesc = ""
                        var tTipo = ""

                        if(dataSnapshot.exists()){

                            if (dataSnapshot.hasChild("nombre")){
                                if(dataSnapshot.child("tipo").value.toString().equals("SUBTEST")){
                                    tName = dataSnapshot.child("nombre").value.toString()
                                    tDesc = dataSnapshot.child("descripcion").value.toString()
                                    tTipo = dataSnapshot.child("tipo").value.toString()
                                    holder.subTestName.text = tName
                                    holder.subTestDesc.text = tDesc
                                }
                                else if(dataSnapshot.child("tipo").value.toString().equals("ITEM")){
                                    tName = dataSnapshot.child("nombre").value.toString()
                                    tTipo = dataSnapshot.child("tipo").value.toString()
                                    holder.subTestName.text = tName
                                }
                                else if(dataSnapshot.child("tipo").value.toString().equals("MENSAJE")){
                                    tName = dataSnapshot.child("nombre").value.toString()
                                    tTipo = dataSnapshot.child("tipo").value.toString()
                                    holder.subTestName.text = tName
                                }
                            }
                        }
                        holder.itemView.setOnClickListener{
                            if (tTipo == "SUBTEST"){
                                val subTestIntent = Intent(applicationContext , EditSubTestActivity::class.java)
                                subTestIntent.putExtra(EditSubTestActivity.SUBTEST_ID, subtestIds)
                                subTestIntent.putExtra(TEST_ID, idTest)
                                subTestIntent.putExtra(TEST_NAME, nameTest)
                                subTestIntent.putExtra(EditSubTestActivity.SUBTEST_NAME, tName)
                                subTestIntent.putExtra(EditSubTestActivity.SUBTEST_DESC, tDesc)
                                startActivity(subTestIntent)
                            }
                        }
                    }

                    override fun onCancelled(databaseError: DatabaseError) {
                        println("loadPost:onCancelled ${databaseError.toException()}")
                    }
                }
                subTestsRef.child(subtestIds).addValueEventListener(listener)
            }
        }
        dataList.adapter = nAdapter
        nAdapter.startListening()


        floatingMenu = findViewById(R.id.famTest)
        fabMessage = findViewById(R.id.fabTestMessage)
        fabSubTest = findViewById(R.id.fabTestSubTest)
        fabSimpleItem = findViewById(R.id.fabTestSimpleItem)


        fabMessage.setOnClickListener {
            val intent = Intent (this, CreateMessageActivity::class.java)
            intent.putExtra(CreateItemActivity.RUTA_BD, "$idTest/Bloques")
            startActivity(intent)
        }

        fabSimpleItem.setOnClickListener {
            val intent = Intent (this, CreateItemActivity::class.java)
            intent.putExtra(CreateItemActivity.RUTA_BD, "$idTest/Bloques")
            intent.putExtra(EditTestActivity.TEST_NAME, nameTest)
            intent.putExtra(EditSubTestActivity.SUBTEST_NAME, "")
            intent.putExtra(EditSubTestActivity.SUBTEST_ID, "")
            intent.putExtra(TEST_ID, idTest)
            startActivity(intent)
        }

        fabSubTest.setOnClickListener {
            val intent = Intent (this, CreateSubTestActivity::class.java)
            intent.putExtra(TEST_ID, idTest)
            intent.putExtra(TEST_NAME, nameTest)
            startActivity(intent)
        }

    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE)
        return if (connectivityManager is ConnectivityManager) {
            val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
            networkInfo?.isConnected ?: false
        } else false
    }

    override fun onStart() {
        super.onStart()

        broadRec = object : BroadcastReceiver() {

            override fun onReceive(context: Context, intent: Intent) {
                if(!isNetworkAvailable())
                {
                    val snackbar = Snackbar.make(findViewById(R.id.editTestView),"Sin conexión, los datos se actualizarán una vez vuelva la conexión", Snackbar.LENGTH_INDEFINITE)
                    snackbar.setAction("Aceptar") { snackbar.dismiss() }

                    snackbar.show()
                }
            }
        }

        registerReceiver(broadRec, IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"))
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(broadRec)
    }

    companion object {
        const val TEST_ID = "com.andes.chameleon.testfolders.ID_TEST"
        const val TEST_NAME = "com.andes.chameleon.testfolders.NAME_TEST"
        const val TEST_DESCRIPTION = "com.andes.chameleon.testfolders.DESCRIPTION_TEST"
    }

    class SubTestsViewHolder(itemView:View): RecyclerView.ViewHolder(itemView) {
        var subTestName : TextView = itemView.findViewById(R.id.subtest_card_name)
        var subTestDesc : TextView = itemView.findViewById(R.id.subtest_card_description)
    }
}
