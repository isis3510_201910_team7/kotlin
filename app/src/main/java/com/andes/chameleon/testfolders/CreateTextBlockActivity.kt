package com.andes.chameleon.testfolders


import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.widget.Button
import android.widget.EditText
import android.widget.Switch
import android.widget.Toast
import com.andes.chameleon.testfolders.DataModel.TextModel
import com.google.firebase.database.FirebaseDatabase

class CreateTextBlockActivity : AppCompatActivity() {

    private lateinit var rutaBD: String
    private lateinit var broadRec: BroadcastReceiver

    private lateinit var texto: EditText
    private lateinit var visibilidad: Switch
    private lateinit var btnCrear: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_text_block)

        rutaBD = intent.getStringExtra(CreateItemActivity.RUTA_BD)

        texto = findViewById(R.id.txtBloqueTexto)
        visibilidad = findViewById(R.id.swVisibilidadBloqueTexto)
        btnCrear = findViewById(R.id.btnCrearBloqueTex)

        btnCrear.setOnClickListener{
            saveText()
        }
    }

    private fun saveText(){
        val text = texto.text.toString().trim()

        if(text == "")
        {
            Toast.makeText(this, "El enunciado no puede estar vacío", Toast.LENGTH_SHORT).show()
            return
        }

        val visibility = visibilidad.isChecked

        val ref = FirebaseDatabase.getInstance().getReference("tests/$rutaBD")
        val textId = ref.push().key

        val nuevoTexto = TextModel(textId!!, text, visibility, TestsActivity.TEXTO)

        ref.child(textId).setValue(nuevoTexto).addOnCompleteListener{
            //Toast.makeText(applicationContext, "¡Texto guardado!", Toast.LENGTH_LONG).show()
        }

        finish()
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE)
        return if (connectivityManager is ConnectivityManager) {
            val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
            networkInfo?.isConnected ?: false
        } else false
    }

    override fun onStart() {
        super.onStart()

        broadRec = object : BroadcastReceiver() {

            override fun onReceive(context: Context, intent: Intent) {
                if(!isNetworkAvailable())
                {
                    val snackbar = Snackbar.make(findViewById(R.id.textBlockView),"Sin conexión, los datos se actualizarán una vez vuelva la conexión", Snackbar.LENGTH_INDEFINITE)
                    snackbar.setAction("Aceptar") { snackbar.dismiss() }

                    snackbar.show()
                }
            }
        }

        registerReceiver(broadRec, IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"))
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(broadRec)
    }

}
