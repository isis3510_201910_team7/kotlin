package com.andes.chameleon.testfolders

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.andes.chameleon.testfolders.DataModel.ProtocolPush
import com.andes.chameleon.testfolders.DataModel.Test
import com.andes.chameleon.testfolders.DataModel.TestPull
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.database.*

class CreateProtocolActivty : AppCompatActivity() {

    private lateinit var testsRef : DatabaseReference
    lateinit var name: EditText
    private lateinit var description: EditText
    private lateinit var btnCrearProtocolo: Button
    lateinit var testList: ArrayList<String>

    private lateinit var broadRec: BroadcastReceiver

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_protocol_activty)

        testList = ArrayList()
        name = findViewById(R.id.txtNombreProtocolo)
        description = findViewById(R.id.txtDescripcionProtocolo)
        btnCrearProtocolo = findViewById(R.id.btnCrearProtocolo)

        val dataList: RecyclerView = findViewById(R.id.add_tests_list)
        dataList.layoutManager = LinearLayoutManager(this)

        testsRef = FirebaseDatabase.getInstance().reference.child("tests")

        val option = FirebaseRecyclerOptions.Builder<TestPull>()
            .setQuery(testsRef, TestPull::class.java)
            .build()

        val nAdapter = object : FirebaseRecyclerAdapter<TestPull, TestsViewHolder>(option){
            override fun onCreateViewHolder(p0: ViewGroup, p1: Int): TestsViewHolder {
                val view : View = LayoutInflater.from(p0.context).inflate(R.layout.protocols_test_card, p0, false)

                return TestsViewHolder(view)
            }

            override fun onBindViewHolder(holder: TestsViewHolder, position: Int, model: TestPull) {
                val testIds : String = getRef(position).key!!
                val listener = object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        if(dataSnapshot.exists()){
                            val tName: String = dataSnapshot.child("nombre").value.toString()
                            holder.testName.text = tName

                            holder.checkBox.setOnCheckedChangeListener{_, isChecked ->
                                if(isChecked){
                                    testList.add(testIds)
                                }
                                else{
                                    testList.remove(testIds)
                                }

                            }
                        }
                    }

                    override fun onCancelled(databaseError: DatabaseError) {
                        println("loadPost:onCancelled ${databaseError.toException()}")
                    }
                }
                testsRef.child(testIds).addValueEventListener(listener)
            }
        }
        dataList.adapter = nAdapter
        nAdapter.startListening()

        btnCrearProtocolo.setOnClickListener{
            createProtocol()
        }

    }

    private fun createProtocol(){
        val nName = name.text.toString().trim()
        val nDescription = description.text.toString().trim()

        when {
            nName == "" -> {
                Toast.makeText(this, "El campo del nombre no puede ser vacío", Toast.LENGTH_SHORT).show()
                return
            }
            nDescription == "" -> {
                Toast.makeText(this, "El campo de descripción no puede ser vacío", Toast.LENGTH_SHORT).show()
                return
            }
            testList.size == 0 -> {
                Toast.makeText(this, "Debe escoger por lo menos un test", Toast.LENGTH_SHORT).show()
                return
            }
            else -> {
                val ref = FirebaseDatabase.getInstance().getReference("protocolos")
                val protocolId = ref.push().key

                val nuevoProtocolo =
                    ProtocolPush(protocolId!!, nName, nDescription)

                ref.child(protocolId).setValue(nuevoProtocolo).addOnCompleteListener{
                    //Toast.makeText(this, "¡Protocolo guardado!", Toast.LENGTH_LONG).show()
                }

                val ref2 = FirebaseDatabase.getInstance().getReference("protocolos/").child("$protocolId/Tests")
                for (i in testList){
                    val protocolId2 = ref2.push().key
                    val nuevoTest =
                        Test(i, "", "", "","")
                    ref2.child(protocolId2!!).setValue(nuevoTest)
                }

                this.finish()
            }
        }
    }


    class TestsViewHolder(itemView:View): RecyclerView.ViewHolder(itemView) {
        var testName : TextView = itemView.findViewById(R.id.protocol_test_card_name)
        var checkBox: CheckBox = itemView.findViewById(R.id.checkBoxTest)
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE)
        return if (connectivityManager is ConnectivityManager) {
            val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
            networkInfo?.isConnected ?: false
        } else false
    }

    override fun onStart() {
        super.onStart()

        broadRec = object : BroadcastReceiver() {

            override fun onReceive(context: Context, intent: Intent) {
                if(!isNetworkAvailable())
                {
                    val snackbar = Snackbar.make(findViewById(R.id.protocolActivity),"Sin conexión, los datos se actualizarán una vez vuelva la conexión", Snackbar.LENGTH_INDEFINITE)
                    snackbar.setAction("Aceptar") { snackbar.dismiss() }

                    snackbar.show()
                }
            }
        }

        registerReceiver(broadRec, IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"))
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(broadRec)
    }
}
