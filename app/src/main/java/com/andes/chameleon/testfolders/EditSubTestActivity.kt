package com.andes.chameleon.testfolders

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.andes.chameleon.testfolders.DataModel.ItemPull
import com.andes.chameleon.testfolders.EditTestActivity.Companion.TEST_ID
import com.andes.chameleon.testfolders.EditTestActivity.Companion.TEST_NAME
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.github.clans.fab.FloatingActionButton
import com.github.clans.fab.FloatingActionMenu
import com.google.firebase.database.*

class EditSubTestActivity : AppCompatActivity() {

    private lateinit var floatingMenu: FloatingActionMenu
    private lateinit var fabMessage: FloatingActionButton
    private lateinit var fabSimpleItem: FloatingActionButton
    private lateinit var idTest: String
    private lateinit var idSubTest: String
    private lateinit var nameTest: String
    private lateinit var nameSubTest: String
    private lateinit var descSubTest: String
    private lateinit var itemsRef : DatabaseReference
    private lateinit var broadRec: BroadcastReceiver

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_sub_test)

        idTest = intent.getStringExtra(TEST_ID)
        idSubTest = intent.getStringExtra(SUBTEST_ID)

        nameTest = intent.getStringExtra(TEST_NAME)
        nameSubTest = intent.getStringExtra(SUBTEST_NAME)
        descSubTest = intent.getStringExtra(SUBTEST_DESC)

        val dataList: RecyclerView = findViewById(R.id.items_list)
        dataList.layoutManager = LinearLayoutManager(this)

        val subtestName = findViewById<TextView>(R.id.subtestName)
        val subtestName2 = findViewById<TextView>(R.id.subtestName2)
        val subTestDescription = findViewById<TextView>(R.id.subtestDescription)
        val testName = findViewById<TextView>(R.id.testName)

        subtestName.text = nameSubTest
        subtestName2.text = nameSubTest
        subTestDescription.text = descSubTest
        testName.text = nameTest

        testName.setOnClickListener{
            this.finish()
        }

        itemsRef = FirebaseDatabase.getInstance().reference.child("tests").child(idTest).child("Bloques").child(idSubTest).child("Bloques")

        val option = FirebaseRecyclerOptions.Builder<ItemPull>()
            .setQuery(itemsRef, ItemPull::class.java)
            .build()

        val nAdapter = object : FirebaseRecyclerAdapter<ItemPull, ItemViewHolder>(option){
            override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ItemViewHolder {
                val view : View = LayoutInflater.from(p0.context).inflate(R.layout.item_card_item, p0, false)

                return ItemViewHolder(view)
            }

            override fun onBindViewHolder(holder: ItemViewHolder, position: Int, model: ItemPull) {

                val itemIds: String = getRef(position).key!!
                var iName = ""
                var iTipo = ""

                val listener = object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        if(dataSnapshot.exists()){

                            if (dataSnapshot.hasChild("nombre")){
                                iName = dataSnapshot.child("nombre").value.toString()
                                iTipo = dataSnapshot.child("tipo").value.toString()
                                holder.itemName.text = iName
                                holder.itemType.text = iTipo
                            }
                        }
                    }

                    override fun onCancelled(databaseError: DatabaseError) {
                        println("loadPost:onCancelled ${databaseError.toException()}")
                    }
                }

                holder.itemView.setOnClickListener{

                    if(holder.itemType.text == TestsActivity.ITEM) {
                        val itemIntent = Intent(applicationContext, EditSimpleItemActivity::class.java)
                        itemIntent.putExtra(SUBTEST_ID, idSubTest)
                        itemIntent.putExtra(TEST_ID, idTest)
                        itemIntent.putExtra(TEST_NAME, nameTest)
                        itemIntent.putExtra(EditSubTestActivity.SUBTEST_NAME, nameSubTest)
                        itemIntent.putExtra(TestsActivity.ITEM_ID, itemIds)
                        itemIntent.putExtra(TestsActivity.ITEM_NAME, iName)
                        itemIntent.putExtra(TestsActivity.ITEM_DESC, iTipo)
                        itemIntent.putExtra(CreateItemActivity.RUTA_BD, "$idTest/Bloques/$idSubTest/Bloques/$itemIds/Bloques")
                        startActivity(itemIntent)
                    }
                }

                /*
                holder.btnEdit.setOnClickListener{
                    if(holder.tipo == TestsActivity.ITEM) {
                        val intent = Intent (applicationContext, EditItemActivity::class.java)
                        intent.putExtra(TestsActivity.ITEM_ID, itemIds)
                        intent.putExtra(CreateItemActivity.RUTA_BD, "$idTest/Bloques/$idSubTest/Bloques")
                        startActivity(intent)
                    }
                    else if(holder.tipo == TestsActivity.MENSAJE)
                    {
                        val intent = Intent(applicationContext, EditMessageActivity::class.java)
                        intent.putExtra(TestsActivity.MENSAJE_ID, itemIds)
                        intent.putExtra(CreateItemActivity.RUTA_BD, "$idTest/Bloques/$idSubTest/Bloques")
                        startActivity(intent)
                    }
                }
                */

                itemsRef.child(itemIds).addValueEventListener(listener)
            }
        }

        dataList.adapter = nAdapter
        nAdapter.startListening()

        floatingMenu = findViewById(R.id.famSubTest)
        fabMessage = findViewById(R.id.fabSubTestMessage)
        fabSimpleItem = findViewById(R.id.fabSubTestSimpleItem)

        fabMessage.setOnClickListener {
            val intent = Intent (this, CreateMessageActivity::class.java)
            intent.putExtra(CreateItemActivity.RUTA_BD, "$idTest/Bloques/$idSubTest/Bloques")
            intent.putExtra(SUBTEST_ID, idSubTest)
            intent.putExtra(TEST_ID, idTest)
            intent.putExtra(TEST_NAME, nameTest)
            intent.putExtra(EditSubTestActivity.SUBTEST_NAME, nameSubTest)
            startActivity(intent)
        }

        fabSimpleItem.setOnClickListener {
            val intent = Intent (this, CreateItemActivity::class.java)
            intent.putExtra(CreateItemActivity.RUTA_BD, "$idTest/Bloques/$idSubTest/Bloques")
            intent.putExtra(SUBTEST_ID, idSubTest)
            intent.putExtra(TEST_ID, idTest)
            intent.putExtra(TEST_NAME, nameTest)
            intent.putExtra(SUBTEST_NAME, nameSubTest)
            startActivity(intent)
        }


    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE)
        return if (connectivityManager is ConnectivityManager) {
            val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
            networkInfo?.isConnected ?: false
        } else false
    }

    override fun onStart() {
        super.onStart()

        broadRec = object : BroadcastReceiver() {

            override fun onReceive(context: Context, intent: Intent) {
                if(!isNetworkAvailable())
                {
                    val snackbar = Snackbar.make(findViewById(R.id.editSubTestView),"Sin conexión, los datos se actualizarán una vez vuelva la conexión", Snackbar.LENGTH_INDEFINITE)
                    snackbar.setAction("Aceptar") { snackbar.dismiss() }

                    snackbar.show()
                }
            }
        }

        registerReceiver(broadRec, IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"))
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(broadRec)
    }

    companion object {
        const val SUBTEST_ID = "com.andes.chameleon.testfolders.SUBTEST_ID"
        const val SUBTEST_NAME = "com.andes.chameleon.testfolders.SUBTEST_NAME"
        const val SUBTEST_DESC = "com.andes.chameleon.testfolders.SUBTEST_DESC"
    }

    class ItemViewHolder(itemView:View): RecyclerView.ViewHolder(itemView) {
        var itemName : TextView = itemView.findViewById(R.id.item_card_name)
        var itemType: TextView = itemView.findViewById(R.id.item_card_type)
    }
}
