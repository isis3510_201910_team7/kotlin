package com.andes.chameleon.testfolders

import android.app.Activity
import android.app.AlertDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.andes.chameleon.testfolders.DataModel.*
import com.andes.chameleon.testfolders.Fragments.ProtocolsFragment
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.database.*

class ChoosePatientActivity : AppCompatActivity() {

    private var REQUEST_APPLY_ITEM = 1
    private var iter = 0
    private var listaTests: ArrayList<String> = ArrayList()
    private var listaElementosItems: ArrayList<Aplicable> = ArrayList()
    private lateinit var broadRec: BroadcastReceiver

    private lateinit var patientsRef : DatabaseReference
    private lateinit var protocolsRef : DatabaseReference
    private lateinit var testsRef : DatabaseReference
    private lateinit var protocolID: String
    private lateinit var protocolName: String

    private lateinit var resId: String

    private lateinit var lobbieId: String
    private var iterItem = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_choose_patient)

        protocolID = intent.getStringExtra(ProtocolsFragment.PROTOCOL_ID)
        protocolName = intent.getStringExtra(ProtocolsFragment.PROTOCOL_NAME)

        val pName1 = findViewById<TextView>(R.id.protocolName)
        pName1.text = protocolName
        val dataList: RecyclerView = findViewById(R.id.choose_patients_list)
        dataList.layoutManager = LinearLayoutManager(this)
        val contexto = this

        patientsRef = FirebaseDatabase.getInstance().reference.child("pacientes")

        val option = FirebaseRecyclerOptions.Builder<PatientPull>()
            .setQuery(patientsRef, PatientPull::class.java)
            .build()

        val nAdapter = object : FirebaseRecyclerAdapter<PatientPull, PatientsViewHolder>(option){
            override fun onCreateViewHolder(p0: ViewGroup, p1: Int): PatientsViewHolder {
                val view : View = LayoutInflater.from(p0.context).inflate(R.layout.choose_patient_card, p0, false)

                return PatientsViewHolder(view)
            }

            override fun onBindViewHolder(holder: PatientsViewHolder, position: Int, model: PatientPull) {

                val pacienteIds : String = getRef(position).key!!
                val listener = object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        val pName: String = dataSnapshot.child("name").value.toString()
                        val pId : String = dataSnapshot.child("id").value.toString()
                        holder.patientName.text = pName
                        holder.patientId.text = pId

                        holder.itemView.setOnClickListener{

                            val ref = FirebaseDatabase.getInstance().getReference("resultados/")
                            resId = ref.push().key!!

                            val nuevoResultado = ResultadoProtocolo(resId, protocolID, pacienteIds)

                            ref.child(resId).setValue(nuevoResultado).addOnCompleteListener{

                            }

                            val ref2 = FirebaseDatabase.getInstance().getReference("lobbies/")

                            lobbieId = "" + (Math.random() * 9999).toInt()

                            val nuevoExamen = Lobby(lobbieId)

                            ref2.child(lobbieId).setValue(nuevoExamen)

                            val builder = AlertDialog.Builder(contexto)

                            builder.setTitle("Número lobby")
                            builder.setMessage("El número de lobby es: $lobbieId")

                            builder.setPositiveButton("Aceptar"){ _, _ ->
                                enviarIntentApply(listaElementosItems[iter])
                            }

                            val dialog: AlertDialog = builder.create()

                            dialog.show()

                        }
                    }

                    override fun onCancelled(databaseError: DatabaseError) {
                        println("loadPost:onCancelled ${databaseError.toException()}")
                    }
                }

                patientsRef.child(pacienteIds).addValueEventListener(listener)
            }

        }

        dataList.adapter = nAdapter
        nAdapter.startListening()

        protocolsRef = FirebaseDatabase.getInstance().reference.child("protocolos").child(protocolID).child("Tests")

        protocolsRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                dataSnapshot.children.forEach{
                    listaTests.add(it.child("id").value.toString())
                }

            }

            override fun onCancelled(error: DatabaseError) {
                // Failed to read value
            }
        })

        testsRef = FirebaseDatabase.getInstance().reference.child("tests")
        testsRef.addValueEventListener(object : ValueEventListener {

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.

                var bloquesTest: Iterable<DataSnapshot>
                var bloquesSubTest: Iterable<DataSnapshot>
                var tipoBloque: String

                dataSnapshot.children.forEach{

                    for (testActual in listaTests){

                        if(testActual == it.child("id").value.toString()){

                            bloquesTest = it.child("Bloques").children

                            bloquesTest.forEach { bloqueActual ->

                                tipoBloque = bloqueActual.child("tipo").value.toString()

                                if(tipoBloque == TestsActivity.SUBTEST)
                                {
                                    bloquesSubTest = bloqueActual.child("Bloques").children

                                    bloquesSubTest.forEach { it3 ->
                                        procesarElemento(it3, testActual, bloqueActual.child("id").value.toString())
                                    }
                                }

                                else
                                {
                                    procesarElemento(bloqueActual, testActual, "")
                                }
                            }
                        }
                    }
                }

            }

            override fun onCancelled(error: DatabaseError) {
                // Failed to read value
            }
        })
    }

    private fun procesarElemento(elemento: DataSnapshot, testActual: String, bloqueActualId: String)
    {
        var map: HashMap<String, String>
        var elementosConfig: Iterable<DataSnapshot>

        var tipoElemento: String
        var idElemento: String
        var visibleElemento: Boolean

        var hayEnunciado = false
        var hayTool = false
        var hayScore = false

        val tipoBloque = elemento.child("tipo").value.toString()

        if(tipoBloque == TestsActivity.ITEM) {

            val nuevoItem = Item (elemento.child("id").value.toString(),
                elemento.child("nombre").value.toString(),
                elemento.child("simpleComplejo").value.toString(),
                elemento.child("tipo").value.toString())

            val nuevoItemCompleto = ItemApply(TestsActivity.ITEM, nuevoItem)

            val bloquesItem = elemento.child("Bloques").children

            bloquesItem.forEach { it4 ->

                tipoElemento = it4.child("tipo").value.toString()
                idElemento = it4.child("id").value.toString()
                visibleElemento = it4.child("visibleExaminado").value.toString().toBoolean()

                when (tipoElemento) {
                    TestsActivity.TEXTO -> {
                        nuevoItemCompleto.enunciado =
                            TextModel(idElemento, it4.child("texto").value.toString(),
                                visibleElemento, TestsActivity.TEXTO)

                        hayEnunciado = true
                    }
                    TestsActivity.QUALIFICATION_TOOL -> {
                        nuevoItemCompleto.tool =
                            QualificationTool(idElemento, it4.child("tipoTool").value.toString(),
                                visibleElemento,TestsActivity.QUALIFICATION_TOOL)

                        hayTool = true
                    }
                    TestsActivity.SCORE -> {

                        map = HashMap()

                        elementosConfig = it4.child("configuracion").children

                        elementosConfig.forEach{ configElement ->
                            map[configElement.key.toString()] = configElement.value.toString()
                        }

                        nuevoItemCompleto.score =
                            Score(idElemento,it4.child("tipoScore").value.toString(),
                                visibleElemento, map, TestsActivity.SCORE)

                        hayScore = true
                    }
                }
            }

            nuevoItemCompleto.idProtocolo = protocolID
            nuevoItemCompleto.idTest = testActual
            nuevoItemCompleto.idSubTest = bloqueActualId

            if(!hayEnunciado) {
                nuevoItemCompleto.enunciado =
                    TextModel("dummy", "No aplica.",
                        false, TestsActivity.TEXTO)

            }

            if(hayTool && hayScore) {
                listaElementosItems.add(nuevoItemCompleto)
            }

        }

        else if (tipoBloque == TestsActivity.MENSAJE)
        {
            val nuevoMensaje = Mensaje (elemento.child("id").value.toString(),
                elemento.child("nombre").value.toString(),
                elemento.child("mensaje").value.toString(),
                elemento.child("visibleExaminado").value.toString().toBoolean(),
                elemento.child("tipo").value.toString())

            val nuevoMensajeAplicar = MensajeApply(TestsActivity.MENSAJE, nuevoMensaje)

            nuevoMensajeAplicar.idProtocolo = protocolID
            nuevoMensajeAplicar.idTest = testActual
            nuevoMensajeAplicar.idSubTest = bloqueActualId

            listaElementosItems.add(nuevoMensajeAplicar)
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(requestCode == REQUEST_APPLY_ITEM && resultCode == Activity.RESULT_OK)
        {
            iter += 1

            if(iter < listaElementosItems.size)
            {
                enviarIntentApply(listaElementosItems[iter])
                if(iter == listaElementosItems.size - 1) finish()
            }
            else
            {
                iter = 0
                iterItem = 0
            }
        }
    }

    private fun enviarIntentApply(aplicable: Aplicable)
    {
        if(aplicable.tipoAplicable == TestsActivity.ITEM)
        {
            val itemActual: ItemApply = aplicable as ItemApply

            val choosePatientIntent = Intent (applicationContext, ApplyActivity::class.java)
            val map = itemActual.score.configuracion
            val arrString = arrayOfNulls<String>(map.size)

            if(itemActual.score.tipoScore == getString(R.string.Textual))
            {
                for (j in 0 until map.size)
                {
                    arrString[j] = map["Opcion$j"]
                }

            }
            else
            {
                arrString[0] = map[getString(R.string.Inicial)]
                arrString[1] = map[getString(R.string.Salto)]
                arrString[2] = map[getString(R.string.Fin)]
            }

            choosePatientIntent.putExtra(CreateItemActivity.RUTA_BD, "resultados/$resId")
            choosePatientIntent.putExtra(EditTestActivity.TEST_ID, itemActual.idTest)
            choosePatientIntent.putExtra(EditSubTestActivity.SUBTEST_ID, itemActual.idSubTest)
            choosePatientIntent.putExtra(TestsActivity.ITEM_ID, itemActual.item.id)
            choosePatientIntent.putExtra(TestsActivity.ITEM_NAME, itemActual.item.nombre)

            choosePatientIntent.putExtra(TestsActivity.TEXTO, itemActual.enunciado.texto)
            choosePatientIntent.putExtra(TestsActivity.QUALIFICATION_TOOL, itemActual.tool.tipoTool)
            choosePatientIntent.putExtra(TestsActivity.SCORE, itemActual.score.tipoScore)
            choosePatientIntent.putExtra(TestsActivity.CONFIGURACION, arrString)
            choosePatientIntent.putExtra(TestsActivity.VISIBLE_EXAMINADO, itemActual.score.visibleExaminado)
            choosePatientIntent.putExtra(TestsActivity.LOBBY, lobbieId)
            choosePatientIntent.putExtra(TestsActivity.ORDEN_ITEM, iterItem)

            val ultimo = iter == listaElementosItems.size - 1

            val nuevoItemLobby = ItemLobby(iterItem, itemActual.enunciado.texto, NO_RESPONDIDO, itemActual.score.visibleExaminado, itemActual.score.tipoScore, itemActual.score.configuracion, ultimo)

            FirebaseDatabase.getInstance().getReference("lobbies/$lobbieId/items/it$iterItem").setValue(nuevoItemLobby)
            iterItem++

            startActivityForResult(choosePatientIntent, REQUEST_APPLY_ITEM)
        }

        else
        {
            val mensajeActual: MensajeApply = aplicable as MensajeApply
            val intent = Intent (applicationContext, ApplyMessageActivity::class.java)

            val msg = mensajeActual.mensaje

            intent.putExtra(TestsActivity.MENSAJE, msg.mensaje)
            intent.putExtra(TestsActivity.NOMBRE_MENSAJE, msg.nombre)

            startActivityForResult(intent, REQUEST_APPLY_ITEM)
        }


    }

    class PatientsViewHolder(itemView:View): RecyclerView.ViewHolder(itemView) {
        val patientName : TextView = itemView.findViewById(R.id.choose_patient_card_name)
        val patientId : TextView = itemView.findViewById(R.id.choose_patient_card_id)
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE)
        return if (connectivityManager is ConnectivityManager) {
            val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
            networkInfo?.isConnected ?: false
        } else false
    }

    override fun onStart() {
        super.onStart()

        broadRec = object : BroadcastReceiver() {

            override fun onReceive(context: Context, intent: Intent) {
                if(!isNetworkAvailable())
                {
                    val snackbar = Snackbar.make(findViewById(R.id.choosePatientView),"Sin conexión, los datos se actualizarán una vez vuelva la conexión", Snackbar.LENGTH_INDEFINITE)
                    snackbar.setAction("Aceptar") { snackbar.dismiss() }

                    snackbar.show()
                }
            }
        }

        registerReceiver(broadRec, IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"))
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(broadRec)
    }

    companion object {
        const val RESPONDIDO = "RESPONDIDO"
        const val NO_RESPONDIDO = "NO_RESPONDIDO"
    }
}
