package com.andes.chameleon.testfolders.Fragments


import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.andes.chameleon.testfolders.CreateTestActivity
import com.andes.chameleon.testfolders.DataModel.TestPull
import com.andes.chameleon.testfolders.EditTestActivity
import com.andes.chameleon.testfolders.R
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.database.*

class TestsFragment : Fragment() {

    private lateinit var testsRef : DatabaseReference

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view : View = inflater.inflate(R.layout.fragment_tests, container, false)
        val dataList: RecyclerView = view.findViewById(R.id.tests_list)
        dataList.layoutManager = LinearLayoutManager(context)

        val addTestBtn = view.findViewById<FloatingActionButton>(R.id.fabAddTest)

        testsRef = FirebaseDatabase.getInstance().reference.child("tests")

        val option = FirebaseRecyclerOptions.Builder<TestPull>()
            .setQuery(testsRef, TestPull::class.java)
            .build()

        val nAdapter = object : FirebaseRecyclerAdapter<TestPull, TestsViewHolder>(option){
            override fun onCreateViewHolder(p0: ViewGroup, p1: Int): TestsViewHolder {
                val view1 : View = LayoutInflater.from(p0.context).inflate(R.layout.test_card_item, p0, false)

                return TestsViewHolder(view1)
            }

            override fun onBindViewHolder(holder: TestsViewHolder, position: Int, model: TestPull) {

                val testIds : String = getRef(position).key!!
                var tName = ""
                var tDesc = ""

                val listener = object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        if(dataSnapshot.exists()){
                            tName = dataSnapshot.child("nombre").value.toString()
                            tDesc = dataSnapshot.child("descripcion").value.toString()
                            holder.testName.text = tName
                            holder.testDesc.text = tDesc
                        }

                        holder.itemView.setOnClickListener{
                            val subTestIntent = Intent (context, EditTestActivity::class.java)
                            subTestIntent.putExtra(EditTestActivity.TEST_ID, testIds)
                            subTestIntent.putExtra(EditTestActivity.TEST_NAME, tName)
                            subTestIntent.putExtra(EditTestActivity.TEST_DESCRIPTION, tDesc)
                            startActivity(subTestIntent)
                        }
                    }

                    override fun onCancelled(databaseError: DatabaseError) {
                        println("loadPost:onCancelled ${databaseError.toException()}")
                    }
                }
                testsRef.child(testIds).addValueEventListener(listener)
            }
        }
        dataList.adapter = nAdapter
        nAdapter.startListening()

        addTestBtn.setOnClickListener {

            startActivity(Intent(context, CreateTestActivity::class.java))
        }

        return view
    }

    interface OnFragmentInteractionListener {
        fun onFragmentInteraction(uri: Uri)
    }


    class TestsViewHolder(itemView:View): RecyclerView.ViewHolder(itemView) {
        var testName : TextView = itemView.findViewById(R.id.test_card_name)
        var testDesc : TextView = itemView.findViewById(R.id.test_card_description)
    }
}
