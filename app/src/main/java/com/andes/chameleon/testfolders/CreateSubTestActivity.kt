package com.andes.chameleon.testfolders

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.andes.chameleon.testfolders.DataModel.Test
import com.google.firebase.database.FirebaseDatabase

class CreateSubTestActivity : AppCompatActivity() {

    private lateinit var nombre: EditText
    private lateinit var descripcion: EditText
    private lateinit var instrucciones: EditText
    private lateinit var btnCrear: Button
    private lateinit var idTestPadre: String
    private lateinit var broadRec: BroadcastReceiver

    private lateinit var testName: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_sub_test)

        nombre = findViewById(R.id.txtNombreSubTest)
        descripcion = findViewById(R.id.txtDescripcionSubTest)
        instrucciones = findViewById(R.id.txtInstruccionesSubTest)

        btnCrear = findViewById(R.id.btnCrearSubTest)
        idTestPadre = intent.getStringExtra(EditTestActivity.TEST_ID)
        testName = intent.getStringExtra(EditTestActivity.TEST_NAME)

        btnCrear.setOnClickListener{
            saveSubTest()
        }

    }

    private fun saveSubTest(){

        val name = nombre.text.toString().trim()
        val descr = descripcion.text.toString().trim()
        val instr = instrucciones.text.toString().trim()

        when {
            name == "" -> {
                Toast.makeText(this, "El campo de nombre no puede ser vacío", Toast.LENGTH_SHORT).show()
                return
            }
            descr == "" -> {
                Toast.makeText(this, "El campo del descripción no puede ser vacío", Toast.LENGTH_SHORT).show()
                return
            }
            instr == "" -> {
                Toast.makeText(this, "El campo de instrucciones no puede ser vacío", Toast.LENGTH_SHORT).show()
                return
            }
            else -> {
                val ref = FirebaseDatabase.getInstance().getReference("tests/$idTestPadre/Bloques")
                val subTestId = ref.push().key
                val nuevoSubTest =
                    Test(subTestId!!, name, descr, instr, TestsActivity.SUBTEST)

                ref.child(subTestId).setValue(nuevoSubTest).addOnCompleteListener{
                    //Toast.makeText(applicationContext, "¡SubTest guardado!", Toast.LENGTH_LONG).show()
                }

                val intent = Intent(this, EditSubTestActivity::class.java)
                intent.putExtra(EditTestActivity.TEST_ID, idTestPadre)
                intent.putExtra(EditSubTestActivity.SUBTEST_NAME, name)
                intent.putExtra(EditSubTestActivity.SUBTEST_ID, subTestId)
                intent.putExtra(EditTestActivity.TEST_NAME, testName)
                intent.putExtra(EditSubTestActivity.SUBTEST_DESC, descr)
                startActivity(intent)

                this.finish()
            }
        }
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE)
        return if (connectivityManager is ConnectivityManager) {
            val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
            networkInfo?.isConnected ?: false
        } else false
    }

    override fun onStart() {
        super.onStart()

        broadRec = object : BroadcastReceiver() {

            override fun onReceive(context: Context, intent: Intent) {
                if(!isNetworkAvailable())
                {
                    val snackbar = Snackbar.make(findViewById(R.id.subtestActivity),"Sin conexión, los datos se actualizarán una vez vuelva la conexión", Snackbar.LENGTH_INDEFINITE)
                    snackbar.setAction("Aceptar") { snackbar.dismiss() }

                    snackbar.show()
                }
            }
        }

        registerReceiver(broadRec, IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"))
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(broadRec)
    }
}
