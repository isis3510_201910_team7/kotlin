package com.andes.chameleon.testfolders.DataModel

//Estructura para crear tests
class Test(val id: String, val nombre: String, val descripcion: String, val instrucciones: String, val tipo: String)