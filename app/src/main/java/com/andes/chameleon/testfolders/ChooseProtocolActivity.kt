package com.andes.chameleon.testfolders

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.andes.chameleon.testfolders.DataModel.*
import com.andes.chameleon.testfolders.Fragments.ExaminedFragment
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.database.*

class ChooseProtocolActivity : AppCompatActivity() {

    private var REQUEST_APPLY_ITEM = 1
    private var iter = 0
    private var listaTests: ArrayList<String> = ArrayList()
    private var listaSubTest: ArrayList<String> = ArrayList()
    private var listaItems: ArrayList<Item> = ArrayList()
    private var listaElementosItems: ArrayList<Aplicable> = ArrayList()
    private lateinit var broadRec: BroadcastReceiver

    private lateinit var protocolsRef : DatabaseReference
    private lateinit var testsRef : DatabaseReference
    private lateinit var patientName: String
    private lateinit var patientID: String
    private lateinit var protocolID: String

    private lateinit var resId: String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_choose_protocol)

        protocolID = ""
        patientID = intent.getStringExtra(ExaminedFragment.EXAMINED_ID)
        patientName = intent.getStringExtra(ExaminedFragment.EXAMINED_NAME)

        val pName1 = findViewById<TextView>(R.id.protocolName)
        pName1.text = patientName
        val dataList: RecyclerView = findViewById(R.id.choose_protocols_list)
        dataList.layoutManager = LinearLayoutManager(this)

        protocolsRef = FirebaseDatabase.getInstance().reference.child("protocolos")

        val option = FirebaseRecyclerOptions.Builder<ProtocolPull>()
            .setQuery(protocolsRef, ProtocolPull::class.java)
            .build()

        val nAdapter = object : FirebaseRecyclerAdapter<ProtocolPull, ProtocolsViewHolder>(option){
            override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ProtocolsViewHolder {
                val view : View = LayoutInflater.from(p0.context).inflate(R.layout.choose_protocol_card, p0, false)

                return ProtocolsViewHolder(view)
            }

            override fun onBindViewHolder(holder: ProtocolsViewHolder, position: Int, model: ProtocolPull) {

                protocolID = getRef(position).key!!
                val listener = object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        val pName: String = dataSnapshot.child("name").value.toString()
                        val pDesc: String = dataSnapshot.child("description").value.toString()
                        holder.protocolName.text = pName
                        holder.protocolDesc.text = pDesc

                        holder.itemView.setOnClickListener{

                            val ref = FirebaseDatabase.getInstance().getReference("resultados/")
                            resId = ref.push().key!!

                            val nuevoResultado = ResultadoProtocolo(resId, protocolID, patientID)

                            ref.child(resId).setValue(nuevoResultado).addOnCompleteListener{

                            }

                            prepararTest()
                        }
                    }

                    override fun onCancelled(databaseError: DatabaseError) {
                        println("loadPost:onCancelled ${databaseError.toException()}")
                    }
                }

                protocolsRef.child(protocolID).addValueEventListener(listener)
            }

        }

        dataList.adapter = nAdapter
        nAdapter.startListening()

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(requestCode == REQUEST_APPLY_ITEM && resultCode == Activity.RESULT_OK)
        {
            iter += 1

            if(iter < listaElementosItems.size)
            {
                enviarIntentApply(listaElementosItems[iter])
            }
            else
            {
                iter = 0
            }
        }
    }

    private fun prepararTest(){

        protocolsRef = FirebaseDatabase.getInstance().reference.child("protocolos").child(protocolID).child("Tests")

        protocolsRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                dataSnapshot.children.forEach{
                    listaTests.add(it.child("id").value.toString())
                }


                //Log.d("Protocol", "Value is: " + dataSnapshot.toString())
                //Log.d("Protocol", "Lista tests: " + listaTests)
            }

            override fun onCancelled(error: DatabaseError) {
                // Failed to read value
                Log.w("Protocol", "Failed to read value.", error.toException())
            }
        })

        testsRef = FirebaseDatabase.getInstance().reference.child("tests")
        testsRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                dataSnapshot.children.forEach{
                    for (t in listaTests){
                        if(t==it.child("id").value.toString()){
                            val bloques = it.child("Bloques").children
                            bloques.forEach{ it2 ->
                                listaSubTest.add(it2.child("id").value.toString())
                                val bloques2 = it2.child("Bloques").children
                                bloques2.forEach{ it3 ->

                                    if(it3.child("tipo").value.toString() == TestsActivity.ITEM)
                                    {
                                        val nuevoItem = Item (it3.child("id").value.toString(), it3.child("nombre").value.toString(),it3.child("simpleComplejo").value.toString(),it3.child("tipo").value.toString())
                                        listaItems.add(nuevoItem)

                                        val nuevoItemCompleto = ItemApply(TestsActivity.ITEM, nuevoItem)

                                        val bloques3 = it3.child("Bloques").children
                                        bloques3.forEach{ it4 ->

                                            val tipoElemento = it4.child("tipo").value.toString()
                                            val idElemento = it4.child("id").value.toString()
                                            val visibleElemento = it4.child("visibleExaminado").value.toString().toBoolean()


                                            when (tipoElemento) {
                                                TestsActivity.TEXTO -> {
                                                    nuevoItemCompleto.enunciado =
                                                        TextModel(idElemento, it4.child("texto").value.toString(), visibleElemento, TestsActivity.TEXTO)
                                                }
                                                TestsActivity.QUALIFICATION_TOOL -> {
                                                    nuevoItemCompleto.tool =
                                                        QualificationTool(idElemento, it4.child("tipoTool").value.toString(),visibleElemento,TestsActivity.QUALIFICATION_TOOL)
                                                }
                                                TestsActivity.SCORE -> {

                                                    val map = HashMap<String, String>()

                                                    val elementosConfig = it4.child("configuracion").children

                                                    elementosConfig.forEach{ configElement ->
                                                        map[configElement.key.toString()] = configElement.value.toString()
                                                    }

                                                    nuevoItemCompleto.score =
                                                        Score(idElemento,it4.child("tipoScore").value.toString(),visibleElemento,map,TestsActivity.SCORE)

                                                }
                                            }
                                        }

                                        nuevoItemCompleto.idProtocolo = protocolID
                                        nuevoItemCompleto.idTest = t
                                        nuevoItemCompleto.idSubTest = it2.child("id").value.toString()
                                        listaElementosItems.add(nuevoItemCompleto)
                                    }

                                }
                            }
                        }
                    }
                }

            }

            override fun onCancelled(error: DatabaseError) {
                // Failed to read value
                //Log.w("Protocol", "Failed to read value.", error.toException())
            }
        })

        //BUG: Lista de elementos es de 0
        enviarIntentApply(listaElementosItems[iter])
    }

    private fun enviarIntentApply(aplicable: Aplicable)
    {
        if(aplicable.tipoAplicable == TestsActivity.ITEM) {
            val itemActual: ItemApply = aplicable as ItemApply

            val chooseProtocolIntent = Intent(applicationContext, ApplyActivity::class.java)
            val map = itemActual.score.configuracion
            val arrString = arrayOfNulls<String>(map.size)

            if (itemActual.score.tipoScore == getString(R.string.Textual)) {
                for (j in 0 until map.size) {
                    arrString[j] = map["Opcion$j"]
                }

            } else {
                arrString[0] = map[getString(R.string.Inicial)]
                arrString[1] = map[getString(R.string.Salto)]
                arrString[2] = map[getString(R.string.Fin)]
            }

            chooseProtocolIntent.putExtra(CreateItemActivity.RUTA_BD, "resultados/$resId")
            chooseProtocolIntent.putExtra(EditTestActivity.TEST_ID, itemActual.idTest)
            chooseProtocolIntent.putExtra(EditSubTestActivity.SUBTEST_ID, itemActual.idSubTest)
            chooseProtocolIntent.putExtra(TestsActivity.ITEM_ID, itemActual.item.id)
            chooseProtocolIntent.putExtra(TestsActivity.ITEM_NAME, itemActual.item.nombre)

            chooseProtocolIntent.putExtra(TestsActivity.TEXTO, itemActual.enunciado.texto)
            chooseProtocolIntent.putExtra(TestsActivity.QUALIFICATION_TOOL, itemActual.tool.tipoTool)
            chooseProtocolIntent.putExtra(TestsActivity.SCORE, itemActual.score.tipoScore)
            chooseProtocolIntent.putExtra(TestsActivity.CONFIGURACION, arrString)

            startActivityForResult(chooseProtocolIntent, REQUEST_APPLY_ITEM)
        }
    }

    class ProtocolsViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val protocolName : TextView = itemView.findViewById(R.id.choose_protocol_card_name)
        val protocolDesc : TextView = itemView.findViewById(R.id.choose_protocol_card_description)
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE)
        return if (connectivityManager is ConnectivityManager) {
            val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
            networkInfo?.isConnected ?: false
        } else false
    }

    override fun onStart() {
        super.onStart()

        broadRec = object : BroadcastReceiver() {

            override fun onReceive(context: Context, intent: Intent) {
                if(!isNetworkAvailable())
                {
                    val snackbar = Snackbar.make(findViewById(R.id.chooseProtocolView),"Sin conexión, los datos se actualizarán una vez vuelva la conexión", Snackbar.LENGTH_INDEFINITE)
                    snackbar.setAction("Aceptar") { snackbar.dismiss() }

                    snackbar.show()
                }
            }
        }

        registerReceiver(broadRec, IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"))
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(broadRec)
    }
}
