package com.andes.chameleon.testfolders

import android.app.DatePickerDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.view.View
import android.widget.*
import com.andes.chameleon.testfolders.DataModel.PatientPush
import com.andes.chameleon.testfolders.Fragments.ExaminedFragment
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_edit_examined.*
import java.text.SimpleDateFormat
import java.util.*

class EditExaminedActivity : AppCompatActivity() , AdapterView.OnItemSelectedListener{

    private lateinit var name: EditText
    private lateinit var birthdate: EditText
    private lateinit var id: TextView
    private lateinit var pic: ImageView
    private var list_of_items = arrayOf("Ninguno", "Educación básica", "Educación media", "Educación superior")
    private var gender_list = arrayOf("Hombre", "Mujer")
    private lateinit var idExaminedPadre: String
    private lateinit var idCedulaExamined: String
    private lateinit var nameExamined: String
    private lateinit var birthdateExamined: String
    private lateinit var educationExamined: String
    private lateinit var genderExamined: String
    private lateinit var broadRec: BroadcastReceiver

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_examined)

        idExaminedPadre = intent.getStringExtra(ExaminedFragment.EXAMINED_ID)
        idCedulaExamined = intent.getStringExtra(ExaminedFragment.EXAMINED_ID_CEDULA)
        nameExamined = intent.getStringExtra(ExaminedFragment.EXAMINED_NAME)
        birthdateExamined = intent.getStringExtra(ExaminedFragment.EXAMINED_BIRTHDATE)
        educationExamined = intent.getStringExtra(ExaminedFragment.EXAMINED_EDUCATION)
        genderExamined = intent.getStringExtra(ExaminedFragment.EXAMINED_GENDER)

        val genderDropDown = findViewById<Spinner>(R.id.genderSpinnerEdit)
        genderDropDown!!.onItemSelectedListener = this
        val educationDropDown = findViewById<Spinner>(R.id.educationSpinner)
        educationDropDown!!.onItemSelectedListener = this

        id = findViewById(R.id.PatientId)
        name = findViewById(R.id.PatientFullName)
        birthdate = findViewById(R.id.PatientBirthdate)
        pic = findViewById(R.id.userIconEdit)

        id.text = idCedulaExamined
        name.setText(nameExamined)
        birthdate.setText(birthdateExamined)

        if(genderExamined == "Masculino" || genderExamined == "Hombre"){
            pic.setImageResource(R.drawable.sample_user)
        } else {
            pic.setImageResource(R.drawable.girl)
        }

        //DropDown
        val aa = ArrayAdapter(this, android.R.layout.simple_spinner_item, list_of_items)
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        educationDropDown.adapter = aa
        educationDropDown.prompt = "Nivel de educación"

        val bb = ArrayAdapter(this, R.xml.spinner_item, gender_list)
        bb.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        genderDropDown.adapter = bb
        genderDropDown.prompt = "Género"

        //Botón Crear
        createPatientBtn1.setOnClickListener{
            editPatient()
        }


        //Date Picker
        val c = Calendar.getInstance()

        val dateSetListener = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            c.set(Calendar.YEAR, year)
            c.set(Calendar.MONTH, monthOfYear)
            c.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            val myFormat = "dd/MM/yyyy" // mention the format you need
            val sdf = SimpleDateFormat(myFormat, Locale.US)
            birthdate.setText(sdf.format(c.time))
        }
        birthdate.setOnClickListener {
            DatePickerDialog(this,
                dateSetListener,
                // set DatePickerDialog to point to today's date when it loads up
                c.get(Calendar.YEAR),
                c.get(Calendar.MONTH),
                c.get(Calendar.DAY_OF_MONTH)).show()
        }

        genderDropDown.onItemSelectedListener= object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (position == 1){
                    pic.setImageResource(R.drawable.girl)
                }
                else{
                    pic.setImageResource(R.drawable.sample_user)
                }
            }

        }
    }

    private fun editPatient() {
        val nId = id.text.toString().trim()
        val nName = name.text.toString().trim()
        val nBirthdate = birthdate.text.toString().trim()
        val date = if(nBirthdate != "") SimpleDateFormat("dd/MM/yyyy").parse(nBirthdate).time else Date().time
        val minDate =  SimpleDateFormat("dd/MM/yyyy").parse("01/01/1900").time
        val nEducation = educationSpinner.selectedItem.toString().trim()

        when {
            nId == "" -> {
                Toast.makeText(this, "El campo de identificación no puede ser vacío", Toast.LENGTH_SHORT).show()
                return
            }
            nName == "" -> {
                Toast.makeText(this, "El campo del nombre no puede ser vacío", Toast.LENGTH_SHORT).show()
                return
            }
            nBirthdate == "" -> {
                Toast.makeText(this, "El campo de fecha de nacimiento no puede ser vacío", Toast.LENGTH_SHORT).show()
                return
            }
            date > Date().time -> {
                Toast.makeText(this, "Fecha inválida", Toast.LENGTH_SHORT).show()
                return
            }
            date < minDate -> {
                Toast.makeText(this, "Fecha inválida", Toast.LENGTH_SHORT).show()
                return
            }
            else -> {
                val ref = FirebaseDatabase.getInstance().getReference("pacientes")


                val nuevoPaciente =
                    PatientPush(nId, nName, nBirthdate, nEducation, "Male")

                ref.child(idExaminedPadre).setValue(nuevoPaciente).addOnCompleteListener{
                }
                this.finish()
            }
        }
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE)
        return if (connectivityManager is ConnectivityManager) {
            val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
            networkInfo?.isConnected ?: false
        } else false
    }

    override fun onStart() {
        super.onStart()

        broadRec = object : BroadcastReceiver() {

            override fun onReceive(context: Context, intent: Intent) {
                if(!isNetworkAvailable())
                {
                    val snackbar = Snackbar.make(findViewById(R.id.editExaminedView),"Sin conexión, los datos se actualizarán una vez vuelva la conexión", Snackbar.LENGTH_INDEFINITE)
                    snackbar.setAction("Aceptar") { snackbar.dismiss() }

                    snackbar.show()
                }
            }
        }

        registerReceiver(broadRec, IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"))
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(broadRec)
    }
    override fun onNothingSelected(parent: AdapterView<*>?) {
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
    }


}
